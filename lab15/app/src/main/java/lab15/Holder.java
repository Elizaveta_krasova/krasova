package lab15;

import java.io.Serializable;

/**
  * This is Holder.
  * We serialize instances of this class.
  * @author Elizaveta Krasova
  * @version %I%, %G%
  */
public class Holder implements Serializable{

    private int id;
    private String name;
    private int phone;
    private static final long serialVersionUID = 10275539472837495L;

    public Holder(int id, String name, int phone) {
        this.id = id;
        this.name = name;
        this.phone = phone;
    }

    /**
      * Using this method, you can get the user ID.
      * @return Holder id
      */
    public int getId() {
        return id;
    }

    /**
      * Using this method, you can get the user name.
      * @return Holder name
      */
    public String getName() {
        return name;
    }

    /**
      * Using this method, you can get the user phone.
      * @return Holder phone
      */
    public int getPhone() {
        return phone;
    }

    /**
      * overridden method toString().
      * @return String
      */
    public String toString(){
        return String.format("%s   %s   %s", this.id, this.name, this.phone);
    }

}
