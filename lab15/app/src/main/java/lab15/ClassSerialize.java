package lab15;

import java.io.*;
import java.util.Scanner;
import java.util.HashMap;
import java.util.Set;

/**
  * This is ClassSerialize.
  * With this class, we store or display the information that is in the file.
  * @author Elizaveta Krasova
  * @version %I%, %G%
  */
class ClassSerialize {

    /**
      * saves information to a file.
      * @params fileName - the path to the file
      */
    public static void fileSerializable(String fileName) {
        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(fileName))) {
            output.writeObject(GeneratedMapping.generateMap());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
      * method reads information from a file.
      * @params fileName - the path to the file
      */
    public static void fileDeserializable(String fileName) {
        HashMap<Integer, Holder> myMap = new HashMap<Integer, Holder>();

        try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(fileName))){
            myMap = (HashMap<Integer, Holder>)input.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");;
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (HashMap.Entry<Integer, Holder> m : myMap.entrySet()) {
            System.out.println(m.getValue());
        }
    }

}
