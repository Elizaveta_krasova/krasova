package lab15;

import java.util.Random;
import java.util.HashMap;

/**
  * This is GeneratedMapping.
  * Here we generate a string and numbers for Map and Map.
  * @author Elizaveta Krasova
  * @version %I%, %G%
  */
public class GeneratedMapping{
    static Random random = new Random();
    /** Maximum value constant for random number generation */
    final static int MAX_INT_VALUES = 1_000;
    /** Generated string length */
    final static int COUNT_SIMBOL = 10;
    /** Start of character a */
    final static int NUMBER_CHAR_A = 97;
    /** The number of characters that make up the string*/
    final static int COUNT_CHAR = 26;
    /** TThe number indicates the number of entries in Map*/
    final static int MAX_MAP_LENGTH = 100;

    /**
      * generates a string of random characters with a set size.
      * @return generated string
      */
    public static String generateString(){
        StringBuilder buffer = new StringBuilder(COUNT_SIMBOL);
        for (int i = 0; i < COUNT_SIMBOL; i++) {
            int randomLimitedInt = NUMBER_CHAR_A + (int)(random.nextFloat() * COUNT_CHAR);
            buffer.append((char)randomLimitedInt);
        }
        return new String(buffer.toString());
    }

    /**
      * generates a random Integer.
      * @return generated Integer
      */
    public static Integer generateInteger(){
        Integer generateInt = random.nextInt(MAX_INT_VALUES);
        return generateInt;
    }

    /**
      * generates a random Map.
      * @return generated Map
      */
    public static HashMap<Integer, Holder> generateMap(){
        HashMap<Integer, Holder> myMap = new HashMap<Integer, Holder>();
        for (int i = 0; i < MAX_MAP_LENGTH; i++) {
            myMap.put(i + 1, new Holder(i + 1, generateString(), generateInteger()));
        }
        return myMap;
    }
}
