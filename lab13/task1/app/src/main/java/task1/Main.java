package task1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;

/**
  * In this class, we check the correctness of the entered phone number
  * and the input to the console continues until the user enters the
  * correct number
  * @author Elizaveta Krasova
  * @version %I%, %G%
  */
public class Main {

    /**
    * The method checks the correctness of the phone number.
    * @return true or false
    */
    public static boolean checkCorrectPhone(String phone) {
        String truePhone = "^(8|\\+7)\\-?((\\(\\d{3}\\))|\\d{3})\\-?\\d{3}\\-?\\d{2}\\-?\\d{2}$";
        return Pattern.compile(truePhone).matcher(phone).matches();
    }

    /**
    * The main method is where we run a loop and it works until
    * the user enters the correct phone number.
    */
    public static void main(String[] args) {
        boolean flag = true;
        Scanner scanner = new Scanner(System.in);
        String phone = "";
        do {
          System.out.println("Enter phone number");
          phone = scanner.nextLine();
        } while (!checkCorrectPhone(phone));
        System.out.println("Your phone " + phone);
    }

}
