package task1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PhoneTest {
    @Test void checkTruePhone1() {
        String number = "89132248984";
        assertTrue(Main.checkCorrectPhone(number), "checking phone number 89132248984");
    }

    @Test void checkTruePhone2() {
        String number = "+79132248984";
        assertTrue(Main.checkCorrectPhone(number), "checking phone number +79132248984");
    }

    @Test void checkTruePhone3() {
        String number = "+7(913)2248984";
        assertTrue(Main.checkCorrectPhone(number), "checking phone number +7(913)2248984");
    }

    @Test void checkTruePhone4() {
        String number = "8(913)224-89-84";
        assertTrue(Main.checkCorrectPhone(number), "checking phone number 8(913)224-89-84");
    }

    @Test void checkFalsePhone5() {
        String number = "8(913) 224-89-84";
        assertFalse(Main.checkCorrectPhone(number), "checking phone number 8(913) 224-89-84");
    }

    @Test void checkFalsePhone6() {
        String number = "8(913)224 89 84";
        assertFalse(Main.checkCorrectPhone(number), "checking phone number 8(913)224 89 84");
    }

    @Test void checkFalsePhone7() {
        String number = "8 913 224 89 84";
        assertFalse(Main.checkCorrectPhone(number), "checking phone number 8 913 224 89 84");
    }

}
