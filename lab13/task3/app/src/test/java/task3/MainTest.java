package task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test void replaceOneBadWord() {
        String badText = "Хочу спать";
        String[] badWords = new String[]{"спать"};
        String goodWord = "учиться";
        String goodText = "Хочу учиться";
        assertEquals(Main.replaceBadWord(badText, badWords, goodWord), goodText);
    }

    @Test void replaceTwoBadWord() {
        String badText = "Хочу спать. Хочу домой";
        String[] badWords = new String[]{"спать", "домой"};
        String goodWord = "учиться";
        String goodText = "Хочу учиться. Хочу учиться";
        assertEquals(Main.replaceBadWord(badText, badWords, goodWord), goodText);
    }

    @Test void stringWithoutBadWord() {
        String badText = "Хочу спать. Хочу домой";
        String[] badWords = new String[]{"работать"};
        String goodWord = "учиться";
        assertEquals(Main.replaceBadWord(badText, badWords, goodWord), badText);
    }

    @Test void emptyGoodWord() {
        String badText = "Хочу спать. Хочу домой";
        String[] badWords = new String[]{"спать"};
        String goodWord = "";
        String goodText = "Хочу . Хочу домой";
        assertEquals(Main.replaceBadWord(badText, badWords, goodWord), goodText);
    }
}
