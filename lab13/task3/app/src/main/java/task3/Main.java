package task3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;

/**
  * This class replaces mentions of words you
  * don't like with the right favorite.
  * @author Elizaveta Krasova
  * @version %I%, %G%
  */
public class Main {

    /**
    * The method replaces bad and unloved words with the right word
    * @params text - The text in which to replace the word
    * @params badWords - Bad words that need to be replaced
    * @params goodWord - Favorite word to replace
    * @return a string with replaced words
    */
    public static String replaceBadWord(String text, String[] badWords, String goodWord) {
        String wordsReplace = "(" + String.join("\\s|\\s", badWords) + ")";
        String wordsReplace2 = "(" + String.join("\\s|", badWords) + ")";
        return Pattern.compile(wordsReplace + "|" + wordsReplace2).
                        matcher(text).replaceAll(" " + goodWord);
    }

    /**
    * The main method that outputs the desired text
    */
    public static void main(String[] args) {
        String text = "я хочу спать я я";
        String[] badWords = new String[]{"хо", "пат", "я"};
        String goodWord = "оно";
        System.out.println(replaceBadWord(text, badWords, goodWord));
    }

}
