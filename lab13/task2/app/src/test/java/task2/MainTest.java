package task2;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test void usualString() {
        String str = "Пятый город, льёт пятью дождями пятый день подряд";
        assertEquals(Main.findNoun(str), "город дождями день ");
    }

    @Test void emptyString() {
        String str = "";
        assertEquals(Main.findNoun(str), "");
    }

    @Test void stringWithoutFifth() {
        String str = "Как прекрасна жизнь";
        assertEquals(Main.findNoun(str), "");
    }

    @Test void stringWithRongWord() {
        String str = "Пятаоо кошка пятыйй день";
        assertEquals(Main.findNoun(str), "");
    }
}
