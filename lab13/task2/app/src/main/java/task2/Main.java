package task2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;


/**
  * This class outputs all nouns that are said to be "fifth".
  * @author Elizaveta Krasova
  * @version %I%, %G%
  */
public class Main {


    /**
    * The method searches for all nouns that are said to be fifth
    * @return a string with all nouns
    */
    public static String findNoun(String text) {
        String trueText = "(\\П|п)\\ят(ый|ая|ое|ые|ого|ому|ом|ой|ую|ою|ых|ым|ыми|ью)\\s" +
        "(\\w+)";
        String result = "";
        Pattern pattern = Pattern.compile(trueText, Pattern.UNICODE_CHARACTER_CLASS);
        Matcher matcher = pattern.matcher(text);

        while(matcher.find()) {
          result += matcher.group(3) + " ";
        }
        return result;
    }

    /**
    * The main method that outputs all nouns.
    */
    public static void main(String[] args) {
      String text = "Пятый город, льёт пятью дождями пятый день подряд";
      System.out.println(findNoun(text));
    }

}
