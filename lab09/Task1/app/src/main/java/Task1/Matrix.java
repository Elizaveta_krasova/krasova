package Task1;

class Matrix{
  public int n;
  public int m;
  public int[][] matrix;

  Matrix(int n, int m){
    this.n = n;
    this.m = m;
    this.matrix = new int[this.n][this.m];
  }

  Matrix(){}

  public void generateMatrix(){
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        this.matrix[i][j] = (int)(Math.random()*10);
      }
    }
  }

  public void setMatrix(int[][] array) throws MatrixException{
    for (int i = 0; i < this.n; i++){
      if (array[i].length != this.m || array.length != this.n){
        throw new MatrixException();
      }
    }
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        this.matrix[i][j] = array[i][j];
      }
    }
  }

  public int[][] getMatrix(){
    return this.matrix;
  }

}
