package Task1;

class MatrixOperation{
  public int[][] result;

  public int[][] matrixSum(Matrix mat1, Matrix mat2) throws NullPointerException, MatrixException{
    int[][] m1 = mat1.getMatrix();
    int[][] m2 = mat2.getMatrix();
    if (m1 == null || m2 == null){
       throw new NullPointerException();
    }
    if (m1.length != m2.length || m1[0].length != m2[0].length){
      throw new MatrixException();
    }
    result = new int[m1.length][m1[0].length];
    for (int i = 0; i < m1.length; i++){
      for (int j = 0; j < m1[0].length; j++){
        result[i][j] = m1[i][j] + m2[i][j];
      }
    }
    return result;
  }

  public int[][] matrixSubtraction(Matrix mat1, Matrix mat2) throws NullPointerException, MatrixException{
    int[][] m1 = mat1.getMatrix();
    int[][] m2 = mat2.getMatrix();
    if (m1 == null || m2 == null){
       throw new NullPointerException();
    }
    if (m1.length != m2.length || m1[0].length != m2[0].length){
      throw new MatrixException();
    }
    result = new int[m1.length][m1[0].length];
    for (int i = 0; i < m1.length; i++){
      for (int j = 0; j < m1[0].length; j++){
        result[i][j] = m1[i][j] - m2[i][j];
      }
    }
    return result;
  }

  public int[][] matrixMultiplication(Matrix mat1, Matrix mat2)  throws NullPointerException, MatrixException{
    int[][] m1 = mat1.getMatrix();
    int[][] m2 = mat2.getMatrix();
    if (m1 == null || m2 == null){
       throw new NullPointerException();
    }
    if (m1[0].length != m2.length){
      throw new MatrixException();
    }
    result = new int[m1.length][m2[0].length];
    for (int i = 0; i < m1.length; i++){
      for (int j = 0; j < m2[0].length; j++){
        for (int z = 0; z < m2.length; z++){
          result[i][j] += m1[i][z] * m2[z][j];
        }
      }
    }
    return result;
  }

  public void printMatrix(Matrix mat1) throws NullPointerException{
    int[][] matrix = mat1.getMatrix();
    if (matrix == null){
      throw new NullPointerException();
    }
    else{
      for (int i = 0; i < matrix.length; i++){
        for (int j = 0; j < matrix[0].length; j++){
          System.out.format("%d ", matrix[i][j]);
        }
        System.out.println();
      }
    }
    System.out.println();
  }

  public void printMatrix(int[][] matrix) throws NullPointerException{
    if (matrix == null){
      throw new NullPointerException();
    }
    else{
      for (int i = 0; i < matrix.length; i++){
        for (int j = 0; j < matrix[0].length; j++){
          System.out.format("%d ", matrix[i][j]);
        }
        System.out.println();
      }
    }
    System.out.println();
  }

}
