package Task1;

class Main{
  public static void main(String[] args) throws MatrixException, NullPointerException{
    MatrixOperation operation = new MatrixOperation();

    int n = 3;
    int m = 3;
    Matrix mat1 = new Matrix(n, m);
    mat1.generateMatrix();
    operation.printMatrix(mat1);

    int n2 = 3;
    int m2 = 3;
    Matrix mat2 = new Matrix(n2, m2);
    mat2.generateMatrix();
    operation.printMatrix(mat2);

    Matrix mat3 = new Matrix(2, 2);
    int[][] array = new int[][]{{1, 2},{3, 4}};
    mat3.setMatrix(array);
    operation.printMatrix(mat3);

    int[][] resSum;
    resSum = operation.matrixSum(mat1, mat2);
    operation.printMatrix(resSum);

    int[][] resMulti;
    resMulti = operation.matrixMultiplication(mat1, mat2);
    operation.printMatrix(resMulti);

    int[][] resSub;
    resSub = operation.matrixSubtraction(mat1, mat2);
    operation.printMatrix(resSub);

  }
}
