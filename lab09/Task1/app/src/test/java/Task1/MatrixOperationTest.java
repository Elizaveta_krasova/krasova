package Task1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MatrixOperationTest {
    @Nested
    @DisplayName("Checking normal operation matrix")
    class ChekingNormalOperationMatrix{
      MatrixOperation operation = new MatrixOperation();
      int[][] m1;
      int[][] m2;
      Matrix matrix1;
      Matrix matrix2;

      @BeforeEach
      void createNewMatrix(){
        m1 = new int[][]{{1,2}, {3,4}, {5,6}};
        m2 = new int[][]{{1,2}, {3,4}, {5,6}};
        matrix1 = new Matrix(3, 2);
        matrix2 = new Matrix(3, 2);
        matrix1.setMatrix(m1);
        matrix2.setMatrix(m2);
      }

      @Test void checkMatrixSumEquals() {
          int[][] res = new int[][]{{2,4}, {6,8}, {10,12}};
          assertArrayEquals(res, operation.matrixSum(matrix1, matrix2), "work correct");
      }

      @Test void checkMatrixSubtractionEquals() {
          int[][] res = new int[][]{{0,0}, {0,0}, {0,0}};
          assertArrayEquals(res, operation.matrixSubtraction(matrix1, matrix2), "work correct");
      }

      @Test void checkMultiplacationSumEquals() {
          int[][] res = new int[][]{{9,12,15}, {19,26,33}, {29,40,51}};
          assertArrayEquals(res, operation.matrixMultiplication(matrix1, matrix2), "work correct");
      }
    }

    @Nested
    @DisplayName("Checking exception in empty matrix")
    class ChekingExceptionInEmptyMatrix{
      MatrixOperation operation = new MatrixOperation();
      Matrix matrix1;
      Matrix matrix2;

      @BeforeEach
      void createNewMatrix(){
        matrix1 = new Matrix();
        matrix2 = new Matrix();
      }

      @Test void checkPrintNullArray() throws NullPointerException{
          try{
            operation.printMatrix(matrix1.getMatrix());
          } catch(NullPointerException e){}
      }

      @Test void checkPrintNullMatrix() throws NullPointerException{
          try{
            operation.printMatrix(matrix1);
          } catch(NullPointerException e){}
      }

      @Test void checkMatrixSumException() throws NullPointerException{
          try{
            operation.matrixSum(matrix1, matrix2);
          } catch(NullPointerException e){}
      }

      @Test void checkMatrixSubtractionException() throws NullPointerException{
          try{
            operation.matrixSubtraction(matrix1, matrix2);
          } catch(NullPointerException e){}
      }

      @Test void checkMatrixMultiplicationException() throws NullPointerException{
          try{
            operation.matrixMultiplication(matrix1, matrix2);
          } catch(NullPointerException e){}
      }
    }

    @Nested
    @DisplayName("Checking exception in different size matrix")
    class ChekingExceptionInDifferentSizeMatrix{
      MatrixOperation operation = new MatrixOperation();
      int[][] m1;
      int[][] m2;
      Matrix matrix1;
      Matrix matrix2;

      @BeforeEach
      void createNewMatrix(){
        m1 = new int[][]{{1,2}, {3,4}, {5,6}};
        m2 = new int[][]{{1}, {2}};
        matrix1 = new Matrix(3, 2);
        matrix2 = new Matrix(1, 1);
        matrix1.setMatrix(m1);
        matrix2.setMatrix(m2);
      }

      @Test void checkMatrixSumException() throws MatrixException{
          try{
            operation.matrixSum(matrix1, matrix2);
          } catch(MatrixException e){}
      }

      @Test void checkMatrixSubtractionException() throws MatrixException{
          try{
            operation.matrixSubtraction(matrix1, matrix2);
          } catch(MatrixException e){}
      }

      @Test void checkMatrixMultiplicationException() throws MatrixException{
          try{
            operation.matrixMultiplication(matrix1, matrix2);
          } catch(MatrixException e){}
      }
    }

}
