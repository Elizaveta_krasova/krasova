package Task1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MatrixOperationTest {

    @Test void checkNormalSetMatrix() throws MatrixException{
        int[][] m1 = new int[][]{{1,2}, {3,4}};
        Matrix matrix = new Matrix(2, 2);
        matrix.setMatrix(m1);
        assertArrayEquals(m1, matrix.getMatrix(), "work correct");
    }

    @Test void checkNullSetMatrix() throws MatrixException{
        Matrix matrix = new Matrix();
        int[][] m1 = new int[][]{};
        try{
          matrix.setMatrix(m1);
        } catch(MatrixException e){}
    }

    @Test void checkSetMatrixDifferentSize() throws MatrixException{
        Matrix matrix = new Matrix(2, 2);
        int[][] m1 = new int[][]{{1,2,3},{4,5,6}};
        try{
          matrix.setMatrix(m1);
        } catch(MatrixException e){}
    }

    @Test void checkSetMatrixDifferentSizeArrayLength() throws MatrixException{
        Matrix matrix = new Matrix(2, 2);
        int[][] m1 = new int[][]{{1,2},{4}};
        try{
          matrix.setMatrix(m1);
        } catch(MatrixException e){}
    }

}
