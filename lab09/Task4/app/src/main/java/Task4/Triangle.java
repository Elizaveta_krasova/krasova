package Task4;

class Triangle extends AllPolygon {

  public int[] lenRib;
  public int perimetr;

  Triangle(int[] lenRib) throws PolygonException{
    super(lenRib);
    this.checkInCorrect();
    if (lenRib.length != 3){
      throw new PolygonException();
    }
    this.perimetr = super.perimeterPolygon();
  }

  public String toString(){
    return "Я треугольник. Мой периметр равен " + this.perimetr;
  }

}
