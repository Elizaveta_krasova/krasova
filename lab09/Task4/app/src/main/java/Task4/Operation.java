package Task4;

class Operation{
  public MyInterface[] allFigures;

  Operation(MyInterface[] allFigures){
    this.allFigures = allFigures;
  }

  public String[] outputAllFigures(){
    String[] array = new String[this.allFigures.length];
    for (int i = 0; i < array.length; i++){
      array[i] = this.allFigures[i].toString();
    }
    return array;
  }

  public void printAllFigures(){
    String[] array = new String[this.allFigures.length];
    for (int i = 0; i < array.length; i++){
      System.out.println(this.allFigures[i].toString());
    }
  }

}
