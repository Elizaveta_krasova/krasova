package Task4;

class AllPolygon implements MyInterface{

    public int[] lenRib;
    public int perimetr;

    AllPolygon(int[] lenRib) throws PolygonException{
      this.lenRib = lenRib;
      this.checkInCorrect();
      this.perimetr = perimeterPolygon();
    }

    public void checkInCorrect() throws PolygonException{
      int sum = 0;
      for (int i = 0; i < this.lenRib.length; i++){
        sum += this.lenRib[i];
      }
      for (int i = 0; i < this.lenRib.length; i++){
        if (2 * this.lenRib[i] > sum){
          throw new PolygonException();
        }
      }
    }

    public int perimeterPolygon(){
      int perimeter = 0;
      for (int i = 0; i < this.lenRib.length; i++){
        perimeter += this.lenRib[i];
      }
      return perimeter;
    }

    public String toString(){
      return "Я " + this.lenRib.length +
      "-угольник. Мой периметр равен " + this.perimetr;
    }

  }
