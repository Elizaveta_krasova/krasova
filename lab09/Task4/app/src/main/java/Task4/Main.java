package Task4;

class Main{

  public static void main(String[] args) throws PolygonException{

    int[] lenRib1 = new int[]{1,2,2};
    Triangle p = new Triangle(lenRib1);
    // System.out.println(p.toString());

    int[] lenRib2 = new int[]{2,3,4};
    Triangle p2 = new Triangle(lenRib2);

    int[] lenRib3 = new int[]{2, 2, 2};
    Triangle p3 = new Triangle(lenRib3);

    int[] lenRib4 = new int[]{1, 2, 2, 2, 5};
    AllPolygon a = new AllPolygon(lenRib4);
    // System.out.println(a.toString());

    Text t = new Text("hello, world");
    Text t2 = new Text("goodbye, world");
    // System.out.println(t.toString());

    MyInterface[] in = new MyInterface[]{p, p2, p3, a, t, t2};
    Operation op = new Operation(in);
    op.printAllFigures();
  }

}
