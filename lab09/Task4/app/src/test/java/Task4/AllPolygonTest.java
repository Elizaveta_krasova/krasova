package Task4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AllPolygonTest {
    @Test void checkInCorrectPolygon() throws PolygonException{
        int[] lenRib = new int[]{1, 2, 2, 2, 5};
        try{
          AllPolygon a = new AllPolygon(lenRib);
        } catch(PolygonException e){}
    }

    @Test void checkInNotCorrectPolygon() throws PolygonException{
        int[] lenRib = new int[]{1, 2, 2, 2, 100};
        try{
          AllPolygon a = new AllPolygon(lenRib);
        } catch(PolygonException e){}
    }

    @Test void checkPerimetrPolygon() throws PolygonException{
        int[] lenRib4 = new int[]{1, 2, 2, 2, 2};
        AllPolygon a = new AllPolygon(lenRib4);
        assertEquals(9, a.perimeterPolygon(), "work correct");
    }

    @Test void checkToStringPolygon() throws PolygonException{
        int[] lenRib4 = new int[]{1, 2, 2, 2, 2};
        AllPolygon a = new AllPolygon(lenRib4);
        assertEquals("Я 5-угольник. Мой периметр равен 9", a.toString(), "work correct");
    }
}
