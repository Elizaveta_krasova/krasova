package Task4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TextTest {

    @Test void checkToStringText() throws PolygonException{
        Text a = new Text("hello");
        assertEquals("hello", a.toString(), "work correct");
    }
}
