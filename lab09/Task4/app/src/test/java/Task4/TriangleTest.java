package Task4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TriangleTest {
    @Test void checkInNotCorrectLengthPolygon() throws PolygonException{
        int[] lenRib = new int[]{1, 2, 2, 2, 5};
        try{
          Triangle a = new Triangle(lenRib);
        } catch(PolygonException e){}
    }

    @Test void checkInNotCorrectSidePolygon() throws PolygonException{
        int[] lenRib = new int[]{1, 2, 10};
        try{
          Triangle a = new Triangle(lenRib);
        } catch(PolygonException e){}
    }

    @Test void checkInCorrectPolygon() throws PolygonException{
        int[] lenRib = new int[]{1, 2, 2};
        try{
          Triangle a = new Triangle(lenRib);
        } catch(PolygonException e){}
    }

    @Test void checkPerimetrPolygon() throws PolygonException{
        int[] lenRib4 = new int[]{1, 2, 2};
        Triangle a = new Triangle(lenRib4);
        assertEquals(5, a.perimeterPolygon(), "work correct");
    }

    @Test void checkToStringPolygon() throws PolygonException{
        int[] lenRib4 = new int[]{1, 2, 2};
        Triangle a = new Triangle(lenRib4);
        assertEquals("Я треугольник. Мой периметр равен 5", a.toString(), "work correct");
    }
}
