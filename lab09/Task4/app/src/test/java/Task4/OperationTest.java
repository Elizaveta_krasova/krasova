package Task4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class OperationTest {

    @Test void checkToStringPolygon() throws PolygonException{
      int[] lenRib1 = new int[]{1,2,2};
      Triangle p = new Triangle(lenRib1);

      int[] lenRib2 = new int[]{2,3,4};
      Triangle p2 = new Triangle(lenRib2);

      int[] lenRib3 = new int[]{2, 2, 2};
      Triangle p3 = new Triangle(lenRib3);

      int[] lenRib4 = new int[]{1, 2, 2, 2, 5};
      AllPolygon a = new AllPolygon(lenRib4);

      Text t = new Text("hello, world");
      Text t2 = new Text("goodbye, world");

      MyInterface[] in = new MyInterface[]{p, p2, p3, a, t, t2};
      Operation op = new Operation(in);
      String[] allFigures = new String[]{
        "Я треугольник. Мой периметр равен 5",
        "Я треугольник. Мой периметр равен 9",
        "Я треугольник. Мой периметр равен 6",
        "Я 5-угольник. Мой периметр равен 12",
        "hello, world",
        "goodbye, world" };
      assertArrayEquals(allFigures, op.outputAllFigures(), "work correct");
    }
}
