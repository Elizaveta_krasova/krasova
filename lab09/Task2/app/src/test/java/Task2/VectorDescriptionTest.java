package Task2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import static org.junit.jupiter.api.Assertions.*;

class VectorDescriptionTest {

    @Nested
    @DisplayName("Check Sum and Substraction Vectors")
    class CheckSumAndSubstraction{

      VectorDescription vector1;
      VectorDescription vector2;

      @BeforeEach
      void createNewVectors() {
        vector1 = new VectorDescription(1, 1, 1, 1);
        vector2 = new VectorDescription(0, 1, 0, 1);
      }

      @Test void checkSumVectorOne() {
          VectorDescription vector_new = vector1.sumVectorOne(vector2);
          assertEquals("Начало вектора (1.0;1.0)" + "\n" +
                       "Конец вектора (1.0;1.0)", vector_new.toString(),"work correct");
      }

      @Test void checkSubtractionVectorOne() {
          VectorDescription vector_new = vector1.subtractionVectorOne(vector2);
          assertEquals("Начало вектора (1.0;1.0)" + "\n" +
                       "Конец вектора (1.0;1.0)",  vector_new.toString(),"work correct");
      }
    }

    @Nested
    @DisplayName("Check Sum and Substraction Vectors Static")
    class CheckSumAndSubstractionStatic{
      VectorDescription vector1;
      VectorDescription vector2;

      @BeforeEach
      void createNewVectors() {
        vector1 = new VectorDescription(1, 1, 1, 1);
        vector2 = new VectorDescription(0, 1, 0, 1);
      }

      @Test void checkSumVectorTwo() {
          VectorDescription vector_new = vector1.sumVectorTwo(vector1, vector2);
          assertEquals("Начало вектора (1.0;1.0)" + "\n" +
                       "Конец вектора (1.0;1.0)", vector_new.toString(),"work correct");
      }

      @Test void checkSubtractionVectorTwo() {
          VectorDescription vector_new = vector1.subtractionVectorTwo(vector1, vector2);
          assertEquals("Начало вектора (1.0;1.0)" + "\n" +
                       "Конец вектора (1.0;1.0)",  vector_new.toString(),"work correct");
      }
    }

    @Nested
    @DisplayName("Check One Vector Operation")
    class CheckInScalar{
      VectorDescription vector1;

      @BeforeEach
      void createNewVectors() {
        vector1 = new VectorDescription(1, 1, 1, 1);
      }

      @Test void checkPrintVector() {
        assertEquals("Начало вектора (1.0;1.0)" + "\n" +
                     "Конец вектора (1.0;1.0)" , vector1.toString(),"work correct");
      }

      @Test void checkMultiplyVectorInScalar() {
          VectorDescription vector_new = vector1.multiplyVectorInScalar(3);
          assertEquals("Начало вектора (1.0;1.0)" + "\n" +
                       "Конец вектора (3.0;3.0)", vector_new.toString(),"work correct");
      }

      @Test void checkDivisionVectorInScalar() {
          VectorDescription vector_new = vector1.divisionVectorInScalar(2);
          assertEquals("Начало вектора (1.0;1.0)" + "\n" +
                       "Конец вектора (0.5;0.5)", vector_new.toString(),"work correct");
      }
    }

}
