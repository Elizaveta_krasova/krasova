package Task2;

class VectorDescription{

  private double x;
  private double y;
  private double x1;
  private double y1;

  VectorDescription(double x, double y, double x1, double y1){
    this.x = x;
    this.y = y;
    this.x1 = x1;
    this.y1 = y1;
  }

  public void setX(double x) {
    this.x = x;
  }

  public double getX() {
    return this.x;
  }

  public void setY(double y) {
    this.y = y;
  }

  public double getY() {
    return this.y;
  }

  public void setX1(double x1) {
    this.x1 = x1;
  }

  public double getX1() {
    return this.x1;
  }

  public void setY1(double y1) {
    this.y1 = y1;
  }

  public double getY1() {
    return this.y1;
  }

  public String toString(){
    return "Начало вектора (" + this.x + ";" + this.y + ")" + "\n" +
           "Конец вектора (" + this.x1 + ";" + this.y1 + ")";
  }

  public VectorDescription sumVectorOne(VectorDescription vector){
    VectorDescription vector_new = new VectorDescription(this.getX(), this.getY(),
    this.getX1() + (vector.getX1() - vector.getX()), this.getY1() + (vector.getY1() - vector.getY()));
    return vector_new;
  }

  public VectorDescription subtractionVectorOne(VectorDescription vector){
    VectorDescription vector_new = new VectorDescription(this.getX(), this.getY(),
    this.getX1() - (vector.getX1() - vector.getX()), this.getY1() - (vector.getY1() - vector.getY()));
    return vector_new;
  }


  public static VectorDescription sumVectorTwo(VectorDescription vector1, VectorDescription vector2){
    VectorDescription vector_new = new VectorDescription(vector1.getX(), vector1.getY(),
    vector1.getX1() + (vector2.getX1() - vector2.getX()), vector1.getY1() + (vector2.getY1() - vector2.getY()));
    return vector_new;
  }

  public static VectorDescription subtractionVectorTwo(VectorDescription vector1, VectorDescription vector2){
    VectorDescription vector_new = new VectorDescription(vector1.getX(), vector1.getY(),
    vector1.getX1() - (vector2.getX1() - vector2.getX()), vector1.getY1() - (vector2.getY1() - vector2.getY()));
    return vector_new;
  }

  public VectorDescription multiplyVectorInScalar(int scalar){
    VectorDescription vector_new = new VectorDescription(this.getX(), this.getY(), this.getX1()*scalar, this.getY1()*scalar);
    return vector_new;
  }

  public VectorDescription divisionVectorInScalar(int scalar){
    VectorDescription vector_new = new VectorDescription(this.getX(), this.getY(), this.getX1()/scalar, this.getY1()/scalar);
    return vector_new;
  }

}
