package Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class EngineerTest {
    Engineer engineer = new Engineer("Bella", "engineer", 15000);

    @Test void checkEngineerWorker() {
        assertEquals("Hello, I am Enginner. Buildings fall because of me ", engineer.engineerWorker(), "work correct");
    }
}
