package Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class WorkerTest {
    Worker worker = new Worker("Bella", "worker", 15000);

    @Test void checkWorkerWorker() {
        assertEquals("Hello, I am Worker. I don't know why I'm here. ", worker.workerWorker(), "work correct");
    }
}
