package Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class ChiefAccountantTest {
    Accountant a = new Accountant("Hora", "loh", 1000);
    ChiefAccountant accountant = new ChiefAccountant("Bella", "Chiefaccountant", 15000);

    @Test void checkAccountantWorker() {
        assertEquals("Hello, I am Cheif Accountant. I can change the salary of employees",
        accountant.chiefAccountantWorker(), "work correct");
    }

    @Test void checkChangeMoney() {
        accountant.changeMoney(a, 15);
        assertEquals(15, a.getMoney(), "work correct");
    }
}
