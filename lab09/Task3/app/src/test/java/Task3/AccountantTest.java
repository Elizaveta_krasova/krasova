package Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AccountantTest {
    Accountant accountant = new Accountant("Bella", "accountant", 15000);
    Accountant accountant2 = new Accountant("Bela", "accountant", 15000);
    Accountant accountant_new = null;
    Employee employee = new Employee("Hardi", "lol", 25000);
    Employee employee_new = null;

    @Test void checkGetName() {
        assertEquals("Bella", accountant.getName(), "work correct");
    }

    @Test void checkGetPosition() {
        assertEquals("accountant", accountant.getPosition(), "work correct");
    }

    @Test void checkGetMoney() {
        assertEquals(15000, accountant.getMoney(), "work correct");
    }

    @Test void checkAccountantWorker() {
        assertEquals("Hello, I am counting money all employee", accountant.accountantWorker(), "work correct");
    }

    @Test void checkEquals() {
        assertEquals(false, accountant.equals(accountant2), "work correct");
    }

    @Test void checkHachCode() {
        assertNotEquals(accountant2.hashCode(), accountant.hashCode(), "work correct");
    }

    @Test void checkOutput() {
        assertEquals("Name employee: Bela Position: accountant Money for work: 15000", accountant2.toString(), "work correct");
    }

    @Test void checkClone() {
      try{
        employee_new = (Employee)employee.clone();
        assertEquals(employee, employee_new, "work correct");
        
        accountant_new = (Accountant)accountant2.clone();
        assertEquals(accountant2, accountant_new, "work correct");
      } catch(CloneNotSupportedException e){
        e.printStackTrace();
      }

    }
}
