package Task3;

public class ChiefAccountant extends Accountant{

  ChiefAccountant(String name, String position, int money){
    super(name, position, money);
  }

  public String chiefAccountantWorker(){
    return "Hello, I am Cheif Accountant. I can change the salary of employees";
  }

  public void changeMoney(Employee employee, int money) {
      employee.setMoney(money);
  }

}
