package Task3;

public class Engineer extends Employee{

  Engineer(String name, String position, int money){
    super(name, position, money);
  }

  public String engineerWorker(){
    return "Hello, I am Enginner. Buildings fall because of me ";
  }

}
