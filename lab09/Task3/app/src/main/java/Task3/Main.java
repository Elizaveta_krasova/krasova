package Task3;

class Main{

  public static void main(String[] args) {
    Employee[] employee = new Employee[4];

    employee[0] = new Accountant("Barni", "Accountant", 20000);
    employee[1] = new ChiefAccountant("Jessika", "CheifAccountant", 30000);
    employee[2] = new Engineer("Retor", "Engineer", 25000);
    employee[3] = new Worker("Danon", "Worker", 15000);

    for (int i = 0; i < employee.length; i++){
      System.out.println(employee[i]);
    }
  }

}
