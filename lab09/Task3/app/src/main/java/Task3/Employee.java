package Task3;

public class Employee implements Cloneable{

  private String name;
  private String position;
  private int money;

  Employee(String name, String position, int money){
    this.name = name;
    this.position = position;
    this.money = money;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public void setPosition(String position) {
    this.position = position;
  }

  public String getPosition() {
    return this.position;
  }

  public void setMoney(int money) {
    this.money = money;
  }

  public int getMoney() {
    return this.money;
  }

  public String toString(){
    return "Name employee: " + this.name +
           " Position: " + this.position +
           " Money for work: " + this.money;
  }

  protected int codeString(String s){
    int d[] = new int[255];
    for (int i = 0; i < s.length(); i++) {
        d[i] = Integer.valueOf(s.charAt(i));
    }
    int code = 0;
    for (int i = 0; i < d.length; i++){
      code += d[i];
    }
    return code;
  }

  public int hashCode() {
    return codeString(this.name) + codeString(this.position) + money;
  }

  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Employee that = (Employee) o;

    if (this.money != that.money) return false;
    if (this.position != that.position) return false;
    return this.name.equals(that.name);
  }

  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

}
