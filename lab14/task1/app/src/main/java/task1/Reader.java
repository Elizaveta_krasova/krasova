package task1;

import java.util.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * class Reader
 */
public class Reader extends Thread {

    /**
     * the book from which we read the data
     */
    private Book book;
    /**
     * current time
     */
    private Long currentTime = null;
    /**
     * latency
     */
    static final int LATENCY = 3000;
    /**
     * latency boundary
     */
    static final int LATENCY_BOUNDARY = 5000;
    /**
     * the file to which the information is output
     */
    static final File FILE_OUTPUT_NAME = new File("/home/elizaveta/krasova/lab14/task1/app/src/main/resources/readers.txt");
    /**
     * the file to which the information2 is output
     */
    static final File FILE_OUTPUT_NAME2 = new File("/home/elizaveta/krasova/lab14/task1/app/src/main/resources/readers2.txt");
    static boolean flag;
    static boolean flag2;
    static FileOutputStream fileOutputStream;
    static FileOutputStream fileOutputStream2;
    /**
     * constructor of the Reader class.
     * accepts a book for reading at the entrance
     * @param - book
     */
    Reader(Book book) {
        this.book = book;
    }

    /**
     * the method that starts the thread
     */
    public void run() {
        String message = " ";
        while (message != ""){
            try {
                if (currentTime != null) {
                    long c = (new Date().getTime() - currentTime);
                    writeInFile(LATENCY, LATENCY_BOUNDARY, c);
                    writeInFile2(c);
                    System.out.println("Ожидаемая задержка - " + LATENCY + " - " + LATENCY_BOUNDARY + ", реальная задержка - " + (new Date().getTime() - currentTime));
                }
                currentTime = new Date().getTime();
                Thread.sleep(LATENCY + new Random().nextInt(LATENCY_BOUNDARY));
                System.out.println("Start reading");
                message = this.book.take();
                System.out.println("I've read " + message);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * a method that writes logs to a file
     * @param a - minimum delay
     * @param b - delay range
     * @param c - real delay
     */
    public static void writeInFile(int a, int b, long c) {
        try {
          if (!flag) {
              flag = true;
              fileOutputStream = new FileOutputStream(FILE_OUTPUT_NAME);
          }
          String text = "Ожидаемая задержка - " + a + " - " + b + ", реальная задержка - " + c + "\n";
          fileOutputStream.write(text.getBytes());
          fileOutputStream.flush();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * a method that writes logs to a file
     * @param c - real delay
     */
    public static void writeInFile2(long c) {
        try {
          if (!flag2) {
              flag2 = true;
              fileOutputStream2 = new FileOutputStream(FILE_OUTPUT_NAME2);
          }
          String text = c + "\n";
          fileOutputStream2.write(text.getBytes());
          fileOutputStream2.flush();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
