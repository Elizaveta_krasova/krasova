package task1;

public class App {
    public static void main(String[] args) {
        Book book = new Book();
        Thread[] threads = {
                new Thread(new Writer(book)),
                new Thread(new Reader(book)),
                new Thread(new Reader(book))
        };

        for (Thread thread : threads){
            thread.start();
        }
    }
}
