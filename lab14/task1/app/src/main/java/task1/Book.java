package task1;

import java.util.concurrent.Semaphore;

/**
 * class Book
 */
public class Book {

    /**
     * field, for data storage
     */
    private String data;
    /**
     * a method that returns data stored in the data string
     * @return - data
     */
    public String getData(){
        return this.data;
    }

    /**
     * A method that writes data to the data field
     * @param data - data to be recorded
     * @throws InterruptedException
     */
    public synchronized void put(String data) throws InterruptedException {
        System.out.println("Data is " + data);
        this.data = data;
        notifyAll();
    }

    /**
     * the method that takes the data
     * @throws InterruptedException
     * @return - data
     */
    public synchronized String take() throws InterruptedException {
        if (this.data == null){
            wait();
        }
        return this.data;
    }
}
