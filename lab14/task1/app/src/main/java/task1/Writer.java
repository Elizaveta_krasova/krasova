package task1;

import java.util.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * class Reader
 */
public class Writer extends Thread {

    /**
     * the book from which we read the data
     */
    private Book book;
    /**
     * current time
     */
    private Long currentTime = null;
    /**
     * latency
     */
    static final int LATENCY = 3000;
    /**
     * latency boundary
     */
    static final int LATENCY_BOUNDARY = 5000;
    /**
     * the file to which the information is input
     */
    static final File FILE_OUTPUT_NAME = new File("/home/elizaveta/krasova/lab14/task1/app/src/main/resources/writer.txt");
    /**
     * the file to which the information2 is input
     */
    static final File FILE_OUTPUT_NAME2 = new File("/home/elizaveta/krasova/lab14/task1/app/src/main/resources/writer2.txt");
    static boolean flag;
    static boolean flag2;
    static FileOutputStream fileOutputStream;
    static FileOutputStream fileOutputStream2;
    /**
     * data for writing to the book
     */
    private String[] someInfo = {"Hello...Hello...Hello...",
            "Is there anybody in there?",
            "Just nod if you can hear me",
            "Is there anyone home?",
            "Come on now",
            "I hear you're feeling down",
            "Well I can ease your pain",
            "Get you on your feet again",
            "Relax",
            "I'll need some information first",
            "Just the basic facts",
            "Can you show me where it hurts?",
            "There is no pain you are receding",
            "A distant ship smoke on the horizon",
            "You are only coming through in waves",
            "Your lips move but I can't hear what you're saying",
            "When I was a child I had a fever",
            "My hands felt just like two balloons",
            "Now I've got that feeling once again",
            "I can't explain you would not understand",
            "This is not how I am",
            "I have become comfortably numb",
            "I have become comfortably numb",
            "Okay (okay, okay, okay)",
            "Just a little pinprick",
            "There'll be no more, ah",
            "But you may feel a little sick",
            "Can you stand up?",
            "I do believe it's working, good",
            "That'll keep you going through the show",
            "Come on it's time to go",
            "There is no pain you are receding",
            "A distant ship, smoke on the horizon",
            "You are only coming through in waves",
            "Your lips move but I can't hear what you're saying",
            "When I was a child",
            "I caught a fleeting glimpse",
            "Out of the corner of my eye",
            "I turned to look but it was gone",
            "I cannot put my finger on it now",
            "The child is grown",
            "The dream is gone",
            "I have become comfortably numb"};

    /**
     * constructor of the Writer class.
     * accepts a book for reading at the entrance
     * @param - book
     */
    Writer(Book book) {
        this.book = book;
    }

    /**
     * the method that starts the thread
     */
    public void run() {
        while (true) {
            try {
                if (currentTime != null) {
                    long c = (new Date().getTime() - currentTime);
                    writeInFile(LATENCY, LATENCY_BOUNDARY, c);
                    writeInFile2(c);
                    System.out.println("Ожидаемая задержка - " + LATENCY + " - " + LATENCY_BOUNDARY + ", реальная задержка - " + (new Date().getTime() - currentTime));
                }
                currentTime = new Date().getTime();
                System.out.println("Start writing");
                this.book.put(this.someInfo[new Random().nextInt(someInfo.length)]);
                System.out.println("Finish writing");
                Thread.sleep(LATENCY + new Random().nextInt(LATENCY_BOUNDARY));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * a method that writes logs to a file
     * @param a - minimum delay
     * @param b - delay range
     * @param c - real delay
     */
    public static void writeInFile(int a, int b, long c) {
        try {
          if (!flag) {
              flag = true;
              fileOutputStream = new FileOutputStream(FILE_OUTPUT_NAME);
          }
          String text = "Ожидаемая задержка - " + a + " - " + b + ", реальная задержка - " + c + "\n";
          fileOutputStream.write(text.getBytes());
          fileOutputStream.flush();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * a method that writes logs to a file
     * @param c - real delay
     */
    public static void writeInFile2(long c) {
        try {
          if (!flag2) {
              flag2 = true;
              fileOutputStream2 = new FileOutputStream(FILE_OUTPUT_NAME2);
          }
          String text = c + "\n";
          fileOutputStream2.write(text.getBytes());
          fileOutputStream2.flush();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
