package task3;

/**
 * resources that smokers may have
 */
public enum Resource {
    MATCHES, TOBACCO, PAPER
}
