package task3;

import java.util.ArrayList;

/**
 * the class of the bartender who serves smokers
 */
public class Barman implements Runnable {
    /**
     * minimum bartender delay
     */
    private static final int LATENCY = 3000;
    /**
     * bartender delay range during service
     */
    private static final int LATENCY_BOUNDARY = 5000;
    /**
     * the table on which the bartender serves
     */
    private Table table;
    /**
     * smokers served by a bartender
     */
    private Smoker[] smokers;
    /**
     * a list that contains all the smokers who are waiting
     */
    private ArrayList<Smoker> waitingSmokers = new ArrayList<>();
    /**
     * class constructor Barman
     * @param table the table served by the bartender
     * @param smokers array Smoker
     */
    Barman(Table table, Smoker[] smokers) {
        this.table = table;
        this.smokers = smokers;
    }

    /**
     * the method updates the smokers' waiting list
     */
    private void updateSmokers() {
        waitingSmokers.clear();
        for (Smoker smoker : smokers) {
            if (smoker.getStatus() == SmokerStatus.WAITING) {
                waitingSmokers.add(smoker);
            }
        }
    }

    /**
     * the method that starts the thread
     */
    public void run() {
        while (true) {
            try {
                ArrayList<Resource> takenResources = new ArrayList<>();
                Thread.sleep(LATENCY + (int) (Math.random() * LATENCY_BOUNDARY));
                updateSmokers();
                if (waitingSmokers.size() >= 2) {
                    System.out.println("Бармен кладет ресурсы");
                    for (int i = 0; i < 2; i++) {
                        takenResources.add(waitingSmokers.get((int)(Math.random() * waitingSmokers.size())).getResource());
                    }
                    table.addResources(takenResources);
                    takenResources.clear();
                } else {
                    System.out.println("Бармен не может взять ресурсы");
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
