package task3;

/**
 * the class of the bartender who serves smokers
 */
public class Smoker implements Runnable {
    /**
     * minimum waiting time
     */
    private static final int LATENCY = 3000;
    /**
     * range delay to wait
     */
    private static final int LATENCY_BOUNDARY = 5000;
    /**
     * minimum delay for smoking
     */
    private static final int LATENCY_FOR_SMOKING = 3000;
    /**
     * smoking delay range
     */
    private static final int LATENCY_BOUNDARY_FOR_SMOKING = 5000;
    /**
     * Smoker's name
     */
    private String name;
    /**
     * A resource that a smoker has
     */
    private Resource resource;
    /**
     * The table where the smoker is sitting
     */
    private Table table;
    /**
     * The status in which the smoker is located
     */
    private SmokerStatus status = SmokerStatus.WAITING;
    /**
     * getting the status
     */
    public SmokerStatus getStatus() {
        return status;
    }
    /**
     * class constructor Barman
     * @param name Smoker's name
     * @param resource   The table where the smoker is sitting
     * @param table the table served by the bartender
     */
    Smoker(String name, Resource resource, Table table) {
        this.name = name;
        this.resource = resource;
        this.table = table;
    }

    /**
     * getting the resource
     * @return Resource
     */
    public synchronized Resource getResource() {
        return resource;
    }

    /**
     * the method looks at the availability of the necessary
     * resources for smoking on the table
     * @return boolean
     */
    public boolean AllResourcesOnTheTable() {
        if (this.resource.equals(Resource.PAPER)) {
            return table.hasMatches() && table.hasTobacco();
        } else if (this.resource.equals(Resource.MATCHES)) {
            return table.hasPaper() && table.hasTobacco();
        } else {
            return table.hasMatches() && table.hasPaper();
        }
    }
    /**
     * takes the necessary resources from the table
     * @return Resources
     */
    public Resource[] takeResourcesFromTable() {
        if (this.resource.equals(Resource.PAPER)) {
            return new Resource[]{table.getMatches(), table.getTobacco()};
        } else if (this.resource.equals(Resource.MATCHES)) {
            return new Resource[]{table.getPaper(), table.getTobacco()};
        } else {
            return new Resource[]{table.getMatches(), table.getPaper()};
        }
    }

    /**
     * the method that starts the thread
     */
    public void run() {
        while (true) {
            try {
                status = SmokerStatus.WAITING;
                Thread.sleep(LATENCY + (int) (Math.random() * LATENCY_BOUNDARY));
                if (AllResourcesOnTheTable()) {
                    takeResourcesFromTable();
                    status = SmokerStatus.SMOKING;
                    System.out.println(this.name + " ушел курить");
                    Thread.sleep(LATENCY_FOR_SMOKING + (int) (Math.random() * LATENCY_BOUNDARY_FOR_SMOKING));
                    System.out.println(this.name + " вернулся");
                } else {
                    System.out.println(this.name + " не нашел нужных ресурсов");
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
