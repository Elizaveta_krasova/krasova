package task3;

import java.util.ArrayList;

/**
 * the class of the bartender who serves smokers
 */
public class Table {
    /**
     * are there any matches
     */
    private boolean hasMatches;
    /**
     * are there any Tobacco
     */
    private boolean hasTobacco;
    /**
     * are there any Paper
     */
    private boolean hasPaper;
    /**
     * getting the matches
     * @return boolean
     */
    public boolean hasMatches() {
        return hasMatches;
    }
    /**
     * getting the Tobacco
     * @return boolean
     */
    public boolean hasTobacco() {
        return hasTobacco;
    }
    /**
     * getting the Paper
     * @return boolean
     */
    public boolean hasPaper() {
        return hasPaper;
    }
    /**
     * a method that takes MATCHES from the table or says that there are none
     * @return MATCHES
     */
    public synchronized Resource getMatches() {
        if (hasMatches()) {
            hasMatches = false;
            System.out.println("Взяли спички");
            return Resource.MATCHES;
        } else {
            System.out.println("Спичек нет");
            return null;
        }
    }
    /**
     * a method that takes TOBACCO from the table or says that there are none
     * @return TOBACCO
     */
    public synchronized Resource getTobacco() {
        if (hasTobacco()) {
            hasTobacco = false;
            System.out.println("Взяли табак");
            return Resource.TOBACCO;
        } else {
            System.out.println("Табака нет");
            return null;
        }
    }
    /**
     * a method that takes PAPER from the table or says that there are none
     * @return PAPER
     */
    public synchronized Resource getPaper() {
        if (hasPaper()) {
            hasPaper = false;
            System.out.println("Взяли бумагу");
            return Resource.PAPER;
        } else {
            System.out.println("Бумаги нет");
            return null;
        }
    }
    /**
     * the method adds resources to the table
     * @param resources
     */
    public void addResources(ArrayList<Resource> resources) {
        for (Resource resource : resources) {
            switch (resource) {
                case MATCHES:
                    if (!hasMatches) {
                        System.out.println("На стол положили спички");
                        hasMatches = true;
                    }
                    break;
                case TOBACCO:
                    if (!hasTobacco) {
                        System.out.println("На стол положили табак");
                        hasTobacco = true;
                    }
                    break;
                case PAPER:
                    if (!hasPaper) {
                        System.out.println("На стол положили бумагу");
                        hasPaper = true;
                    }
                    break;
            }
        }
    }

}
