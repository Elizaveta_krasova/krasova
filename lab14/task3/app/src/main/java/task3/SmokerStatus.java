package task3;

/**
 * smokers' statuses
 */
public enum SmokerStatus {
    WAITING, SMOKING
}
