package task4;

/**
 * a class that implements the behavior of a philosopher
 */
public class Philosopher implements Runnable {
    /**
     * minimum delay for philosopher's lunch
     */
    private static final int LATENCY = 5000;
    /**
     * the delay range for the philosopher's lunch
     */
    private static final int LATENCY_BOUNDARY = 3000;
    /**
     * minimum delay for taking a fork
     */
    private static final int LATENCY_FORK = 500;
    /**
     * the delay range for taking a fork
     */
    private static final int LATENCY_BOUNDARY_FORK = 300;
    /**
     * the status in which the philosopher is located
     */
    public PhilosophersStatus status = PhilosophersStatus.THINK;
    /**
     * the philosopher's left fork
     */
    public Fork leftFork;
    /**
     * the philosopher's right fork
     */
    public Fork rightFork;
    /**
     * the waiter who serves philosophers
     */
    public Waiter waiter;
    /**
     * client's name
     */
    public String name;

    /**
     * constructor of the Philosopher class
     * @param name client's name
     * @param leftFork the philosopher's left fork
     * @param rightFork the philosopher's right fork
     * @param waiter the waiter who serves philosophers
     */
    Philosopher(String name, Fork leftFork, Fork rightFork, Waiter waiter) {
        this.name = name;
        this.leftFork = leftFork;
        this.rightFork = rightFork;
        this.waiter = waiter;
    }

    /**
     * the method that starts the thread
     */
    public void run() {
        while (true) {
            try {
                this.waiter.accessToFork();
                if (this.leftFork.isAvailable() && this.rightFork.isAvailable()) {
                    Thread.sleep(LATENCY_FORK + (int)(Math.random() * LATENCY_BOUNDARY_FORK));
                    this.leftFork.takeFork();
                    System.out.println(this.name + " взял левую вилку");

                    Thread.sleep(LATENCY_FORK + (int)(Math.random() * LATENCY_BOUNDARY_FORK));
                    this.rightFork.takeFork();
                    System.out.println(this.name + " взял правую вилку");

                    System.out.println(this.name + " начал трапезу");
                    this.status = PhilosophersStatus.EAT;
                    Thread.sleep(LATENCY + (int)(Math.random() * LATENCY_BOUNDARY));
                    System.out.println(this.name + " закончил кушать лапшу");

                    Thread.sleep(LATENCY_FORK + (int)(Math.random() * LATENCY_BOUNDARY_FORK));
                    this.leftFork.putFork();
                    System.out.println(this.name + " положил левую вилку");

                    Thread.sleep(LATENCY_FORK + (int)(Math.random() * LATENCY_BOUNDARY_FORK));
                    this.rightFork.putFork();
                    System.out.println(this.name + " положил правую вилку");

                    System.out.println(this.name + " начал думать");
                    this.status = PhilosophersStatus.THINK;
                }
                this.waiter.giveFork();
            } catch (InterruptedException e) {}
        }
    }
}
