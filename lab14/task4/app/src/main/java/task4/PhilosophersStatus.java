package task4;

/**
 * statuses that a philosopher can accept
 */
public enum PhilosophersStatus {
    THINK, EAT
}
