package task4;

import java.util.concurrent.Semaphore;

/**
 * fork class
 */
public class Fork {
    /**
     * semaphore for tracking whether the fork is busy
     */
    Semaphore accessToFork = new Semaphore(1, true);
    /**
     * method to take a fork
     */
    public void takeFork() {
        try {
            this.accessToFork.acquire();
        } catch (InterruptedException e) {}
    }
    /**
     * method to put a fork
     */
    public void putFork() {
        this.accessToFork.release();
    }
    /**
     * the method looks at whether the fork is free
     * @return boolean
     */
    public boolean isAvailable(){
        return this.accessToFork.availablePermits() > 0;
    }
}
