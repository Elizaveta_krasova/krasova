package task4;
import java.util.concurrent.Semaphore;

/**
 * a class that implements the behavior of a waiter who serves philosophers
 */
public class Waiter {
    /**
     * the number of forks you need to eat
     */
    private static final int COUNT_FORK = 2;
    /**
     * semaphore for using forks
     */
    public Semaphore forks;
    /**
     * constructor of the Philosopher class
     * @param countFork number of forks on the table
     */
    Waiter(int countFork) {
        this.forks = new Semaphore(countFork, true);
    }
    /**
     * method to take forks from the table
     */
    public void accessToFork() {
        try {
            this.forks.acquire(COUNT_FORK);
        } catch (InterruptedException e) {}
    }
    /**
     * method to put the forks back on the table
     */
    public void giveFork() {
        this.forks.release(COUNT_FORK);
    }
}
