package task2;

/**
 * a class that implements the behavior of the
 * chair on which clients get their hair cut
 */
public class HaircutChair {
    /**
     * chair status
     */
    public HaircutChairStatus status = HaircutChairStatus.FREE;
    /**
     * the client sits on the barber's chair
     */
    public synchronized void takeSeat(Client client) {
        if (status.equals(HaircutChairStatus.BUSY)) {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println(client.name + " сидит в кресле");
        status = HaircutChairStatus.BUSY;
    }
    /**
     * chair release method
     */
    public synchronized void free() {
        System.out.println("Кресло освободилось");
        status = HaircutChairStatus.FREE;
        notifyAll();
    }

}
