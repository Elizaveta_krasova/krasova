package task2;

/**
 * a class that implements client behavior
 */
public class Client implements Runnable {
    /**
     * the barber who will cut the client's hair
     */
    private Hairdresser hairdresser;
    /**
     * the reception which the client enters
     */
    private Reception reception;
    /**
     * client's name
     */
    public String name;
    /**
     * constructor of the Client class
     * @param name client's name
     * @param hairdresser the barber who will cut the client's hair
     * @param reception the reception which the client enters
     */
    Client(String name, Hairdresser hairdresser, Reception reception) {
        this.name = name;
        this.hairdresser = hairdresser;
        this.reception = reception;
    }
    /**
     * the method that starts the thread
     */
    public void run() {
        if (hairdresser.status.equals(HairdresserStatus.SLEEP)) {
            System.out.println(this.name + " разбудил парикмахера");
            hairdresser.wakeUp(this);
        } else {
            System.out.println(this.name + " идёт в приёмную");
            if (reception.sitChair(this)) {
                System.out.println(this.name + " садится на стул и ждёт");
            } else {
                System.out.println(this.name + " ушел, нет свободного стула");
            }
        }
    }
}
