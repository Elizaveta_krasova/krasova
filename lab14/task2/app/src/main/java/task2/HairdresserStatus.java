package task2;
/**
 * statuses that a hairdresser can accept
 */
public enum HairdresserStatus {
    SLEEP, WORK
}
