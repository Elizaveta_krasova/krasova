package task2;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * a class that implements the behavior of a Reception
 */
public class Reception {
    /**
     * maximum number of chairs at the reception
     */
    private static final int MAX_CHAIRS_IN_RECEPTION = 3;
    /**
     * the queue of customers who are sitting on the reception chairs
     */
    ArrayBlockingQueue<Client> сlientsOnChairs = new ArrayBlockingQueue<>(MAX_CHAIRS_IN_RECEPTION);
    /**
     * the method returns the value of whether the client has sat on a chair
     * @return boolean
     */
    public synchronized boolean sitChair(Client client) {
        return сlientsOnChairs.offer(client);
    }
    /**
     * the method returns the client who first entered the queue
     * @return Client
     */
    public synchronized Client getClient() {
        return сlientsOnChairs.poll();
    }

}
