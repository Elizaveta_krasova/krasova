package task2;

/**
 * a method that implements the behavior of a hairdresser
 */
public class Hairdresser implements Runnable {
    /**
     * minimum delay when cutting
     */
    private static final int LATENCY = 3000;
    /**
     * the range of delay during shearing
     */
    private static final int LATENCY_BOUNDARY = 5000;
    /**
     * Hairdresser status
     */
    public HairdresserStatus status;
    /**
     * reception of the salon where the barber cuts hair
     */
    public Reception reception;
    /**
     * the chair on which the clients of this hairdresser sit
     */
    public HaircutChair haircutChair;
    /**
     * constructor of the Hairdresser class
     * @param reception reception of the salon where the barber cuts hair
     * @param haircutChair the chair on which the clients of this hairdresser sit
     */
    Hairdresser(Reception reception, HaircutChair haircutChair) {
        this.reception = reception;
        this.haircutChair = haircutChair;
    }
    /**
     * the method where the client wakes up the sleeping Hairdresser
     */
    public synchronized void wakeUp(Client client) {
        status = HairdresserStatus.WORK;
        haircut(client);
        this.notify();
    }
    /**
     * the method in which the barber cuts the client's hair
     */
    private void haircut(Client client) {
        try {
            System.out.println("Парикмахер стрижёт " + client.name);
            haircutChair.takeSeat(client);
            Thread.sleep(LATENCY + (int) (Math.random() * LATENCY_BOUNDARY));
            System.out.println("Парикмахер закончил стрижку " + client.name);
            haircutChair.free();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    /**
     * the method that starts the thread
     */
    public synchronized void run() {
        while (true) {
            System.out.println("Парикмахер идёт в приёмную и смотрит");
            if (reception.сlientsOnChairs.size() > 0) {
                status = HairdresserStatus.WORK;
                haircut(reception.getClient());
            } else {
                System.out.println("Клиентов нет, парикмахер пошёл спать");
                status = HairdresserStatus.SLEEP;
                try {
                    this.wait();
                } catch (InterruptedException e) {}
            }
        }
    }

}
