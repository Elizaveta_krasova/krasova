package task2;
/**
 * statuses that a barber chair can accept
 */
public enum HaircutChairStatus {
    BUSY, FREE
}
