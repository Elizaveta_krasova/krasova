package task2;

public class App {

    public static void main(String[] args) {
        final int LATENCY = 300;
        final int LATENCY_BOUNDARY = 500;
        Reception reception = new Reception();
        HaircutChair haircutChair = new HaircutChair();
        Hairdresser hairdresser = new Hairdresser(reception, haircutChair);

        (new Thread(hairdresser)).start();

        int i = 1;
        while (true) {
            try {
                Thread.sleep(LATENCY + (int) (Math.random() * LATENCY_BOUNDARY));
                (new Thread(new Client("Клиент " + i, hairdresser, reception))).start();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            i++;
        }
    }
}
