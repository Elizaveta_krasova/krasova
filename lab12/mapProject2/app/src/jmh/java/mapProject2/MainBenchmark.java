package mapProject2;

import java.util.concurrent.TimeUnit;

import mapProject2.MapClass;
import java.util.*;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.annotations.Level;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
public class MainBenchmark {

      public GeneratedMapping generated = new GeneratedMapping();
      public String[] array = new String[30_000];
      MapClass mymap;
      MapClass myHashMap = new MapClass(new HashMap<String, Long>());
      MapClass myHashtable = new MapClass(new Hashtable<String, Long>());
      MapClass myLinkedHashMap = new MapClass(new LinkedHashMap<String, Long>());
      MapClass myTreeMap = new MapClass(new TreeMap<String, Long>());


      public MapClass createMapAndString(Map<String, Long> newMap) {
          for (int i = 0; i < array.length; i++){
            array[i] = generated.generateString();
          }
          return new MapClass(newMap);
      }

      @Setup(Level.Trial) @Benchmark
      public void testByCreateAndFindSumHashMap() {
          mymap = createMapAndString(new HashMap<String, Long>());
          mymap.sumMapValuesByKeysFromArray(array);
      }

      @Setup(Level.Trial) @Benchmark
      public void testByCreateAndFindSumHashtable() {
          mymap = createMapAndString(new Hashtable<String, Long>());
          mymap.sumMapValuesByKeysFromArray(array);
      }

      @Setup(Level.Trial) @Benchmark
      public void testByCreateAndFindSumLinkedHashMap() {
          mymap = createMapAndString(new LinkedHashMap<String, Long>());
          mymap.sumMapValuesByKeysFromArray(array);
      }

      @Setup(Level.Trial) @Benchmark
      public void testByCreateAndFindSumTreeMap() {
          mymap = createMapAndString(new TreeMap<String, Long>());
          mymap.sumMapValuesByKeysFromArray(array);
      }

      @Setup(Level.Trial) @Benchmark
      public void testByCreateHashMap() {
          mymap = createMapAndString(new HashMap<String, Long>());
      }

      @Setup(Level.Trial) @Benchmark
      public void testByCreateHashtable() {
          mymap = createMapAndString(new Hashtable<String, Long>());
      }

      @Setup(Level.Trial) @Benchmark
      public void testByCreateLinkedHashMap() {
          mymap = createMapAndString(new LinkedHashMap<String, Long>());
      }

      @Setup(Level.Trial) @Benchmark
      public void testByCreateTreeMap() {
          mymap = createMapAndString(new TreeMap<String, Long>());
      }

      @Setup(Level.Trial) @Benchmark
      public void testByFindSumHashMap() {
          myHashMap.sumMapValuesByKeysFromArray(array);
      }

      @Setup(Level.Trial) @Benchmark
      public void testByFindSumHashtable() {
          myHashtable.sumMapValuesByKeysFromArray(array);
      }

      @Setup(Level.Trial) @Benchmark
      public void testByFindSumLinkedHashMap() {
          myLinkedHashMap.sumMapValuesByKeysFromArray(array);
      }

      @Setup(Level.Trial) @Benchmark
      public void testByFindSumTreeMap() {
          myTreeMap.sumMapValuesByKeysFromArray(array);
      }


}
