package mapProject2;

import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.Map;

/**
  * This is MapClass.
  * This class creates an Map from randomly generated
    strings and numbers and searches for the sum of the display
    values corresponding to the rows from the array.
  * @author Elizaveta Krasova
  * @version %I%, %G%
  */
public class MapClass{
  /** Map Storage Field */
  Map<String, Long> myMap;
  /** The size of the generated Map */
  final int SIZE_MAP = 2_000_000;
  /** The number to convert from milliseconds to seconds */
  final int TRANSLATION_IN_SECONDS = 1_000;

  /**
  * Constructor - creating a new object.
  * @params myMap - class Map
  */
  MapClass(Map<String, Long> myMap) {
    this.myMap = myMap;
    insertValuesInMap();
  }

  /**
  * A method for inserting values into a Map by generating a random
    string and a number.
  */
  public void insertValuesInMap() {
    GeneratedMapping generated = new GeneratedMapping();
    long start = System.currentTimeMillis();
    for (int i = 0; i < SIZE_MAP; i++){
      this.myMap.put(generated.generateString(), generated.generateLong());
    }
    double finish = (double)(System.currentTimeMillis() - start)/TRANSLATION_IN_SECONDS;
    System.out.println("Random: " + finish + " sec");
  }

  /**
  * The method searches for the sum of the display values corresponding
    to the rows from the array.
  * @params array - accepts an array of strings as input
  * @return the sum of the values
  */
  public long sumMapValuesByKeysFromArray(String[] array) {
    long start = System.currentTimeMillis();
    long sum = 0L;
    for (String str : array) {
      if (this.myMap.containsKey(str)) {
        sum += this.myMap.get(str);
      }
    }
    double finish = (double)(System.currentTimeMillis() - start)/TRANSLATION_IN_SECONDS;
    System.out.println("Search: " + finish + " sec");
    return sum;
  }
}
