package mapProject2;

import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.Map;

class Main{

  public static void getTime(int sum, int current){
    System.out.println("All: " + ((double)(sum/current)/1000) + " sec");
  }

  public static void main(String[] args) {
    long start;
    MapClass h;
    int sum = 0;
    int current = 1;
    Map<String, Long> myMap;
    GeneratedMapping generated = new GeneratedMapping();
    String[] array = new String[30_000];
    long sumValues = 0L;

    for (int i = 0; i < array.length; i++){
      array[i] = generated.generateString();
    }

    System.out.println("HashMap: ");
    for (int i = 0; i < current; i++){
      start = System.currentTimeMillis();
      h = new MapClass(new HashMap<String, Long>());
      sumValues = h.sumMapValuesByKeysFromArray(array);
      sum += System.currentTimeMillis() - start;
    }
    getTime(sum, current);
    System.out.println("Sum Values: " + sumValues);
    sum = 0;
    sumValues = 0L;


    System.out.println("\n" + "Hashtable: ");
    for (int i = 0; i < current; i++){
      start = System.currentTimeMillis();
      h = new MapClass(new Hashtable<String, Long>());
      sumValues = h.sumMapValuesByKeysFromArray(array);
      sum += System.currentTimeMillis() - start;
    }
    getTime(sum, current);
    System.out.println("Sum Values: " + sumValues);
    sum = 0;
    sumValues = 0L;


    System.out.println("\n" + "LinkedHashMap: ");
    for (int i = 0; i < current; i++){
      start = System.currentTimeMillis();
      h = new MapClass(new LinkedHashMap<String, Long>());
      sumValues = h.sumMapValuesByKeysFromArray(array);
      sum += System.currentTimeMillis() - start;
    }
    getTime(sum, current);
    System.out.println("Sum Values: " + sumValues);
    sum = 0;
    sumValues = 0L;


    System.out.println("\n" + "TreeMap: ");
    for (int i = 0; i < current; i++){
      start = System.currentTimeMillis();
      h = new MapClass(new TreeMap<String, Long>());
      sumValues = h.sumMapValuesByKeysFromArray(array);
      sum += System.currentTimeMillis() - start;
    }
    getTime(sum, current);
    System.out.println("Sum Values: " + sumValues);
    sum = 0;
    sumValues = 0L;

  }
}
