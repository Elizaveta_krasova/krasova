package task1;

import java.util.concurrent.TimeUnit;

import task1.MapClass;

import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.Hashtable;
import java.util.ArrayList;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.annotations.Level;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
public class MainBenchmark {

      MapClass myHashMapForSort = new MapClass(new HashMap<String, Integer>());
      MapClass myHashTableForSort = new MapClass(new Hashtable<String, Integer>());
      MapClass myLinkedHashMapForSort = new MapClass(new LinkedHashMap<String, Integer>());
      MapClass myTreeMapForSort = new MapClass(new TreeMap<String, Integer>());

      @Setup(Level.Trial) @Benchmark
      public void testByInsertHashMap() {
          MapClass myHashMap = new MapClass(new HashMap<String, Integer>());
      }

      @Setup(Level.Trial) @Benchmark
      public void testBySortHashMap() {
          myHashMapForSort.sortMap();
      }


      @Setup(Level.Trial) @Benchmark
      public void testByInsertHashTable() {
          MapClass myHashTable = new MapClass(new Hashtable<String, Integer>());
      }

      @Setup(Level.Trial) @Benchmark
      public void testBySortHashTable() {
          myHashTableForSort.sortMap();
      }

      @Setup(Level.Trial) @Benchmark
      public void testByInsertLinkedHashMap() {
          MapClass myLinkedHashMap = new MapClass(new LinkedHashMap<String, Integer>());
      }

      @Setup(Level.Trial) @Benchmark
      public void testBySortLinkedHashMap() {
          myLinkedHashMapForSort.sortMap();
      }

      @Setup(Level.Trial) @Benchmark
      public void testByInsertTreeMap() {
          MapClass myTreeMap = new MapClass(new TreeMap<String, Integer>());
      }

      @Setup(Level.Trial) @Benchmark
      public void testBySortTreeMap() {
          myTreeMapForSort.sortMap();
      }

      @Setup(Level.Trial) @Benchmark
      public void testByInsertAndSortHashMap() {
          MapClass mymap = new MapClass(new HashMap<String, Integer>());
          mymap.sortMap();
      }

      @Setup(Level.Trial) @Benchmark
      public void testByInsertAndSortHashTable() {
          MapClass mymap = new MapClass(new Hashtable<String, Integer>());
          mymap.sortMap();
      }

      @Setup(Level.Trial) @Benchmark
      public void testByInsertAndSortLinkedHashMap() {
          MapClass mymap = new MapClass(new LinkedHashMap<String, Integer>());
          mymap.sortMap();
      }

      @Setup(Level.Trial) @Benchmark
      public void testByInsertAndSortTreeMap() {
          MapClass mymap = new MapClass(new TreeMap<String, Integer>());
          mymap.sortMap();
      }

}
