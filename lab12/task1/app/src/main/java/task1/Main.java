package task1;

import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.Map;

class Main{

  public static void getTime(int sum, int current){
    System.out.println("All: " + ((double)(sum/current)/1_000) + " sec");
  }

  public static void main(String[] args) {
    long start;
    MapClass h;
    int sum = 0;
    int current = 1;
    int TRANSLATION_IN_SECONDS = 1_000;

    System.out.println("HashMap: ");
    for (int i = 0; i < current; i++){
      start = System.currentTimeMillis();

      h = new MapClass(new HashMap<String, Integer>());
      System.out.println("Random: " + (double)(System.currentTimeMillis() - start)/
                                                  TRANSLATION_IN_SECONDS + " sec");

      long startSort = System.currentTimeMillis();
      h.sortMap();
      System.out.println("Sort: " + (double)(System.currentTimeMillis() - startSort)/
                                                  TRANSLATION_IN_SECONDS + " sec");

      sum += System.currentTimeMillis() - start;
    }
    getTime(sum, current);
    sum = 0;

    System.out.println("\n" + "Hashtable: ");
    for (int i = 0; i < current; i++){
      start = System.currentTimeMillis();
      h = new MapClass(new Hashtable<String, Integer>());
      System.out.println("Random: " + (double)(System.currentTimeMillis() - start)/
                                                  TRANSLATION_IN_SECONDS + " sec");

      long startSort = System.currentTimeMillis();
      h.sortMap();
      System.out.println("Sort: " + (double)(System.currentTimeMillis() - startSort)/
                                                  TRANSLATION_IN_SECONDS + " sec");

      sum += System.currentTimeMillis() - start;
    }
    getTime(sum, current);
    sum = 0;

    System.out.println("\n" + "LinkedHashMap: ");
    for (int i = 0; i < current; i++){
      start = System.currentTimeMillis();
      h = new MapClass(new LinkedHashMap<String, Integer>());
      System.out.println("Random: " + (double)(System.currentTimeMillis() - start)/
                                                  TRANSLATION_IN_SECONDS + " sec");

      long startSort = System.currentTimeMillis();
      h.sortMap();
      System.out.println("Sort: " + (double)(System.currentTimeMillis() - startSort)/
                                                  TRANSLATION_IN_SECONDS + " sec");

      sum += System.currentTimeMillis() - start;
    }
    getTime(sum, current);
    sum = 0;

    System.out.println("\n" + "TreeMap: ");
    for (int i = 0; i < current; i++){
      start = System.currentTimeMillis();
      h = new MapClass(new TreeMap<String, Integer>());
      System.out.println("Random: " + (double)(System.currentTimeMillis() - start)/
                                                  TRANSLATION_IN_SECONDS + " sec");

      long startSort = System.currentTimeMillis();                                            
      h.sortMap();
      System.out.println("Sort: " + (double)(System.currentTimeMillis() - startSort)/
                                                  TRANSLATION_IN_SECONDS + " sec");

      sum += System.currentTimeMillis() - start;
    }
    getTime(sum, current);
  }
}
