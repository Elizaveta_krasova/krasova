package task1;

import java.util.Random;

/**
  * This is GeneratedMapping.
  * Here we generate a string and numbers for Map.
  * @author Elizaveta Krasova
  * @version %I%, %G%
  */
public class GeneratedMapping{
  static Random random = new Random();
  /** Maximum value constant for random number generation */
  final static int MAX_INT_VALUES = 1_000_000;
  /** Generated string length */
  final static int COUNT_SIMBOL = 10;
  /** Start of character a */
  final static int NUMBER_CHAR_A = 97;
  /** The number of characters that make up the string*/
  final static int COUNT_CHAR = 26;

  /**
    * generates a string of random characters with a set size.
    * @return generated string
    */
  public String generateString(){
    StringBuilder buffer = new StringBuilder(COUNT_SIMBOL);
    for (int i = 0; i < COUNT_SIMBOL; i++) {
        int randomLimitedInt = NUMBER_CHAR_A + (int)(random.nextFloat() * COUNT_CHAR);
        buffer.append((char)randomLimitedInt);
    }
    return new String(buffer.toString());
  }

  /**
    * generates a random Integer.
    * @return generated Integer
    */
  public Integer generateInteger(){
    Integer generateInt = random.nextInt(MAX_INT_VALUES);
    return generateInt;
  }
}
