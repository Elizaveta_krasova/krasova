package task1;

import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.Map;

/**
  * This is MapClass.
  * This class creates an Map from randomly generated
    strings and numbers and sorts by values.
  * @author Elizaveta Krasova
  * @version %I%, %G%
  */
public class MapClass{
  /** Map Storage Field */
  Map<String, Integer> myMap;
  /** The size of the generated Map */
  final int SIZE_MAP = 2_000_000;

  /**
    * Constructor - creating a new object.
    * @params myMap - class Map
    */
  public MapClass(Map<String, Integer> myMap){
    this.myMap = myMap;
    insertMapValues();
  }

  /**
    * A method for inserting values into a Map by generating a random string and a number.
    */
  public void insertMapValues() {
    GeneratedMapping generated = new GeneratedMapping();
    for (int i = 0; i < SIZE_MAP; i++){
      this.myMap.put(generated.generateString(), generated.generateInteger());
    }
  }

  /**
    * Method for sorting map by values.
    */
  public void sortMap(){
    Integer[] values = (Integer[])this.myMap
                        .values().toArray(new Integer[0]);
    quickSort(values, 0, values.length - 1);
  }

  /**
    * Method for Quick sorting.
    * @params arr[] - Array to sort
    * @params begin - which element to start sorting from
    * @params end - to which element to sort
    */
  public void quickSort(Integer arr[], int begin, int end) {
    if (begin < end) {
        int partitionIndex = partition(arr, begin, end);

        quickSort(arr, begin, partitionIndex - 1);
        quickSort(arr, partitionIndex + 1, end);
    }
  }

  /**
    * Part of the quick sort method.
    * @params arr[] - Array to sort
    * @params begin - which element to start sorting from
    * @params end - to which element to sort
    * @return element index
    */
  private int partition(Integer arr[], int begin, int end) {
    int pivot = arr[end];
    int i = (begin - 1);

    for (int j = begin; j < end; j++) {
        if (arr[j] <= pivot) {
            i++;

            int swapTemp = arr[i];
            arr[i] = arr[j];
            arr[j] = swapTemp;
        }
    }
    int swapTemp = arr[i + 1];
    arr[i + 1] = arr[end];
    arr[end] = swapTemp;
    return i + 1;
  }

}
