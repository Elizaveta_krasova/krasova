package binaryHeap;

class Main{

  public static void main(String[] args) {
    Heap heap = new Heap(31);
    BinaryHeap h1 = new BinaryHeap(120);
    BinaryHeap h2 = new BinaryHeap(40);
    BinaryHeap h3 = new BinaryHeap(50);
    BinaryHeap h4 = new BinaryHeap(80);
    BinaryHeap h5 = new BinaryHeap(20);
    BinaryHeap h6 = new BinaryHeap(100);
    BinaryHeap h7 = new BinaryHeap(150);
    BinaryHeap h8 = new BinaryHeap(30);
    BinaryHeap h9 = new BinaryHeap(210);
    BinaryHeap h10 = new BinaryHeap(180);
    BinaryHeap h11 = new BinaryHeap(10);
    BinaryHeap h12 = new BinaryHeap(90);
    heap.add(h1);
    heap.add(h2);
    heap.add(h3);
    heap.add(h4);
    heap.add(h5);
    heap.add(h6);
    heap.add(h7);
    heap.add(h8);
    heap.add(h9);
    heap.add(h10);
    heap.add(h11);
    heap.add(h12);
    System.out.println(heap.toArray());
    System.out.println(heap.toString());
    heap.pop();
    System.out.println(heap.toString());
    heap.remove(h1);
    System.out.println(heap.toString());
    System.out.println(heap.peek().getValue());
    System.out.println(heap.size());
    System.out.println(heap.isEmpty());
    System.out.println(heap.contains(h10));
    System.out.println(heap.equals(h3, h3));
    heap.iterator();
    heap.clear();
    System.out.println(heap.toString());
    System.out.println(heap.isEmpty());
  }

}
