package binaryHeap;
import java.util.Collection;
import java.util.Iterator;

public class Heap implements Collection{

  private BinaryHeap[] heapArray;
  private int maxSize;
  private int currentSize;

  public Heap(int maxSize){
    this.maxSize = maxSize;
    this.currentSize = 0;
    heapArray = new BinaryHeap[maxSize];
  }

  public BinaryHeap[] toArray(){
   return heapArray;
  }

  public BinaryHeap[] toArray(Object[] c){
   return heapArray;
  }

  public BinaryHeapIterator iterator(){
    BinaryHeapIterator itr = this.new HeapIterator();
    return itr;
  }

  interface BinaryHeapIterator extends java.util.Iterator<BinaryHeap>{ }

  private class HeapIterator implements BinaryHeapIterator{
    private int nextIndex = 0;

    public boolean hasNext() {
        return (nextIndex <= currentSize - 1);
    }

    public BinaryHeap next() {
        BinaryHeap retValue = heapArray[nextIndex];
        nextIndex += 1;
        return retValue;
    }
  }

  public boolean add(Object newNode) {
    if (currentSize == maxSize) {
       return false;
    }
    heapArray[currentSize] = (BinaryHeap) newNode;
    displaceUp(currentSize++);// сортировка (переносим вершину вверх)
    return true;
 }

  private void displaceUp(int index) {
     int parentIndex = (index - 1) / 2;
     BinaryHeap bottom = heapArray[index];
     while (index > 0 && heapArray[parentIndex].getValue() > bottom.getValue()) {
        heapArray[index] = heapArray[parentIndex];
        index = parentIndex;
        parentIndex = (parentIndex - 1) / 2;
     }
     heapArray[index] = bottom;
  }

  public boolean remove(Object element) {
   if (contains(element)) {
      int index = 0;
      for (int i = 0; i < currentSize; i++){
         if (element == heapArray[i]){
           index = i;
           break;
         }
      }
      BinaryHeap root = heapArray[index];
      heapArray[index] = heapArray[--currentSize];
      heapArray[currentSize] = null;
      displaceDown(index);
      return true;
     }
   return false;
  }

  public BinaryHeap pop() {
    BinaryHeap root = heapArray[0];
    heapArray[0] = heapArray[--currentSize];
    heapArray[currentSize] = null;
    displaceDown(0);
    return root;
  }

  public BinaryHeap peek() {
    return heapArray[0];
  }

  private void displaceDown(int index) {
     int largerChild;
     BinaryHeap top = heapArray[index];
     while (index < currentSize / 2) {
        int leftChild = 2 * index + 1;
        int rightChild = leftChild + 1;

        if (rightChild > currentSize && heapArray[leftChild].getValue() > heapArray[rightChild].getValue()) {
           largerChild = rightChild;
        }
        else {
           largerChild = leftChild;
        }

        if (top.getValue() <= heapArray[largerChild].getValue()) {
           break;
        }

        heapArray[index] = heapArray[largerChild];
        index = largerChild;
     }
     heapArray[index] = top;
  }

  public boolean contains(Object element){
    for (int i = 0; i < currentSize; i++){
      if (element == heapArray[i]){
        return true;
      }
    }
    return false;
  }

  public void clear(){
    for (int i = 0; i < currentSize; i++){
      heapArray[i] = null;
    }
  }

  public int size(){
    return (heapArray[0] == null) ? 0 : currentSize;
  }

  public boolean retainAll(Collection c) throws UnsupportedOperationException{
    throw new UnsupportedOperationException();
  }

  public boolean removeAll(Collection c) throws UnsupportedOperationException{
    throw new UnsupportedOperationException();
  }

  public boolean addAll(Collection c) throws UnsupportedOperationException{
    throw new UnsupportedOperationException();
  }

  public boolean containsAll(Collection c) throws UnsupportedOperationException{
    throw new UnsupportedOperationException();
  }

  public boolean isEmpty(){
    return (heapArray[0] == null);
  }

  public int hashCode(BinaryHeap element) {
    return element.getValue() * 12;
  }

  public boolean equals(BinaryHeap element1, BinaryHeap element2){
    if (element1 == element2) return true;
    if (element1 == null || element2.getClass() != element1.getClass()) return false;
    return element1.getValue() == element2.getValue();
  }

  public String toString(){
    String s = "";
    if (heapArray[0] == null){
       return "binary heap is empty";
    }
    for (int i = 0; i < currentSize - 1; i++){
      s = s + String.valueOf(heapArray[i].getValue()) + ", ";
    }
    s += String.valueOf(heapArray[currentSize - 1].getValue());
    return s;
  }

}
