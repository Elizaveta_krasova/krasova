package binaryHeap;

class BinaryHeap{
  private int value;

  public BinaryHeap(int value) {
     this.value = value;
  }

  public void setValue(int value) {
     this.value = value;
  }

  public int getValue() {
     return this.value;
  }

}
