package binaryHeap;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;
import java.util.Iterator;

class HeapTest {
    @Nested
    @DisplayName("Checking a full heap")
    class ChekingFullHeap{
      Heap heap;
      BinaryHeap h1;
      BinaryHeap h2;
      BinaryHeap h3;
      BinaryHeap h4;
      BinaryHeap h5;
      BinaryHeap h6;
      BinaryHeap h7;
      BinaryHeap h8;
      BinaryHeap h9;
      BinaryHeap h10;
      BinaryHeap h11;
      BinaryHeap h12;

      @BeforeEach
      void createNewHeap() {
        heap = new Heap(31);
        h1 = new BinaryHeap(120);
        h2 = new BinaryHeap(40);
        h3 = new BinaryHeap(50);
        h4 = new BinaryHeap(80);
        h5 = new BinaryHeap(20);
        h6 = new BinaryHeap(100);
        h7 = new BinaryHeap(150);
        h8 = new BinaryHeap(30);
        h9 = new BinaryHeap(210);
        h10 = new BinaryHeap(180);
        h11 = new BinaryHeap(10);
        h12 = new BinaryHeap(90);
        heap.add(h1);
        heap.add(h2);
        heap.add(h3);
        heap.add(h4);
        heap.add(h5);
        heap.add(h6);
        heap.add(h7);
        heap.add(h8);
        heap.add(h9);
        heap.add(h10);
        heap.add(h11);
        heap.add(h12);
      }

      @Test void checkToString() {
          assertEquals("10, 20, 50, 40, 30, 90, 150, 120, 210, 180, 80, 100",
                                              heap.toString(), "work correct");
      }

      @Test void checkRemove(){
          assertEquals(true, heap.remove(h1), "work correct");
          assertEquals(false, heap.remove(h1), "work correct");
          assertEquals("10, 20, 50, 40, 30, 90, 150, 100, 210, 180, 80",
                                              heap.toString(), "work correct");
      }

      @Test void checkPop(){
          assertEquals(10, heap.pop().getValue(), "work correct");
          assertEquals("20, 40, 50, 100, 30, 90, 150, 120, 210, 180, 80",
                                              heap.toString(), "work correct");
      }

      @Test void checkPeek(){
          assertEquals(10, heap.peek().getValue(), "work correct");
      }

      @Test void checkSize(){
          assertEquals(12, heap.size(), "work correct");
      }

      @Test void checkIsEmpty(){
          assertEquals(false, heap.isEmpty(), "work correct");
      }

      @Test void checkContainsTrue(){
          assertEquals(true, heap.contains(h10), "work correct");
      }

      @Test void checkContainsFalse(){
          assertEquals(false, heap.contains(new Object()), "work correct");
      }

      @Test void checkEquals(){
          assertEquals(false, heap.equals(h3, h10), "work correct");
          assertEquals(true, heap.equals(h3, h3), "work correct");
      }

      @Test void checkClear(){
          heap.clear();
          assertEquals(0, heap.size(), "work correct");
      }

      @Test void checkIterator(){
          int[] arr1 = new int[]{10, 20, 50, 40, 30, 90, 150, 120, 210, 180, 80, 100};
          int[] arr = new int[12];
          Iterator iterator = heap.iterator();
          int i = 0;
          while (iterator.hasNext()) {
            BinaryHeap retValue = (BinaryHeap)iterator.next();
            arr[i] = retValue.getValue();
            i++;
          }
          assertArrayEquals(arr, arr1, "work correct");
      }
    }

    @Nested
    @DisplayName("Checking is empty heap")
    class ChekingEmptyHeap{
      Heap heap = new Heap(31);

      @Test void checkIsEmpty(){
          assertEquals(true, heap.isEmpty(), "work correct");
      }

      @Test void checkSize(){
          assertEquals(0, heap.size(), "work correct");
      }

      @Test void checkToString() {
          assertEquals("binary heap is empty", heap.toString(), "work correct");
      }

      @Test void checkIterator(){
          int[] arr1 = new int[]{};
          int[] arr = new int[0];
          Iterator iterator = heap.iterator();
          int i = 0;
          while (iterator.hasNext()) {
            BinaryHeap retValue = (BinaryHeap)iterator.next();
            arr[i] = retValue.getValue();
            i++;
          }
          assertArrayEquals(arr, arr1, "work correct");
      }
    }

    @Nested
    @DisplayName("Checking exception")
    class ChekingException{
      Heap heap = new Heap(31);
      Heap heap2 = new Heap(31);

      @Test void checkExceptionContainsAll() throws UnsupportedOperationException{
        try{
            heap.containsAll(heap2);
        } catch(UnsupportedOperationException e){}
      }

      @Test void checkExceptionAddAll() throws UnsupportedOperationException{
        try{
            heap.addAll(heap2);
        } catch(UnsupportedOperationException e){}
      }

      @Test void checkExceptionRemoveAll() throws UnsupportedOperationException{
        try{
            heap.removeAll(heap2);
        } catch(UnsupportedOperationException e){}
      }

      @Test void checkExceptionRetainAll() throws UnsupportedOperationException{
        try{
            heap.retainAll(heap2);
        } catch(UnsupportedOperationException e){}
      }

    }

}
