package task2.DTO;

public class HolderWrapper{

    public String name;
    public String phone;
    public String equipmentTitle;
    public String equipmentColor;
    public String storageCellCode;
    public String storageCellCapacity;

    public HolderWrapper(String name, String phone, String equipmentTitle,
    String equipmentColor, String storageCellCode, String storageCellCapacity) {
        this.name = name;
        this.phone = phone;
        this.equipmentTitle = equipmentTitle;
        this.equipmentColor = equipmentColor;
        this.storageCellCode = storageCellCode;
        this.storageCellCapacity = storageCellCapacity;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getEquipmentTitle() {
        return equipmentTitle;
    }

    public String getEquipmentColor() {
        return equipmentColor;
    }

    public String getStorageCellCode() {
        return storageCellCode;
    }

    public String getStorageCellCapacity() {
        return storageCellCapacity;
    }

    public String toString() {
        return this.name + "  " + this.phone + "  " + this.equipmentTitle + "  " +
        this.equipmentColor + "  " + this.storageCellCode + "  " + this.storageCellCapacity;
    }

}
