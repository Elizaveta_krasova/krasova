<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="app.Holder" %>
<html>
  <body bgcolor="#FFA07A" align="center">
    <h1 style="color: #800000;">Вы десериализовали</h1>
    <p style="color: #800000;">
      <% Holder holder = (Holder) request.getAttribute("holder"); %>
      <% if (holder != null) {%>
        <%= holder.getId() %>
        <%= holder.getName() %>
        <%= holder.getPhone() %>
      <%}%>
    </p>

    <form method="get" action="/asd/holders_serialization">
        <button type="submit" style="color: #800000;">Вернуться обратно</button>
    </form>
  </body>
</html>
