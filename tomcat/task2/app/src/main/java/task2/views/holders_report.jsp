<%@ page import="app.HolderWrapper" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<body bgcolor="#800000">
    <h1 align="center" style="color: #ffc0cb;">Отчёт</h1>
    <form action="/college/holders_report" method="get">
        <% String phone = request.getParameter("phone"); %>
        <p align="center" style="color: #ffc0cb;">
            phone: <input name="phone" required value=<%= phone != null ? phone : "" %>>
            <br><br>
            <input align="center" style="color: #800000;" type="submit" value="Тыкать сюда"/>
        </p>
    </form>

    <% HolderWrapper[] wrappers = (HolderWrapper[]) request.getAttribute("DTO"); %>
    <% if (wrappers.length != 0) { %>
      <p align="center">
      <table border=1 rules="rows" style="color: #ffc0cb;">
          <tr>
              <th>id</th>
              <th>Имя Фамилия</th>
              <th>Телефон</th>
              <th>Предмет</th>
              <th>Цвет</th>
              <th>Код</th>
              <th>Вместимость</th>
          </tr>

          <% for (HolderWrapper wrapper : wrappers) { %>
          <tr>
              <td align="center"><%= wrapper.getId() %></td>
              <td align="center"><%= wrapper.getName() %></td>
              <td align="center"><%= wrapper.getPhone() %></td>
              <td align="center"><%= wrapper.getEquipmentTitle() %></td>
              <td align="center"><%= wrapper.getEquipmentColor() %></td>
              <td align="center"><%= wrapper.getStorageCellCode() %></td>
              <td align="center"><%= wrapper.getStorageCellCapacity() %></td>
          </tr>
          <%}%>
      </table>
      </p>
    <%}%>
</body>
</html>
