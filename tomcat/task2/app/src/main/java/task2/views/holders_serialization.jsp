<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="app.Holder" %>
<body bgcolor="#FFA07A">
<h1 align="center" style="color: #800000;">Таблица</h1>
  <p align="center">
    <table border=1 rules="rows" style="color: #800000;">
        <tr>
            <th>id</th>
            <th>Имя</th>
            <th>Показать</th>
            <th>Сериализовать</th>
        </tr>
        <%for (Holder holder : Holder.getAll()) {%>
        <tr>
            <td ><%= holder.getId() %></td>
            <td><%= holder.getName() %></td>
            <td>
                <form action="/asd/holder_deserialization/<%= holder.getId() %>" method="get">
                    <button type="submit" style="color: #800000;">тык, чтобы показать</button>
                </form>
            </td>
            <td>
                <form action="/asd/holder_serialize/<%= holder.getId() %>" method="post">
                    <button type="submit" style="color: #800000;">тык, чтобы сериализовать</button>
                </form>
            </td>
        </tr>
        <%}%>
    </table>
  </p>
</body>
