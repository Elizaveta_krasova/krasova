<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page import="app.Holder" %>
<%Holder[] holders = (Holder[]) request.getAttribute("holders"); %>
<body bgcolor="#ffc0cb">
  <h1 align="center"><%= request.getAttribute("text")%></h1>
  <% if (holders != null) { %>
    <%for (Holder h : holders){%>
    	<p align="center">
        <%= h.getName()%>
        <%= h.getPhone()%>
      </p>
    <%}%>
  <%}%>
</body>
