package task2.Query;

import java.sql.SQLException;
import java.util.ArrayList;

public class QueryObject extends ConnectionDB{

    public static String holderQuery = "select phone    as holder_phone,\n" +
            "       name     as holder_name,\n" +
            "       equipment_title,\n" +
            "       color    as equipment_color,\n" +
            "       code     as storage_cell_code,\n" +
            "       capacity as storage_cell_capacity\n" +
            "from holder\n" +
            "         left join equipment_to_holder on phone = equipment_to_holder.holder_phone\n" +
            "         left join equipment on equipment_title = title\n" +
            "         left join storage_cell on phone = storage_cell.holder_phone\n" +
            "where phone = '%s';";

    private static ConnectionDB<HolderWrapper> conn = new ConnectionDB<>();

    public static ArrayList<HolderWrapper> getHolderQuery(String phone) {
        try {
            return conn.executeSelect(String.format(holderQuery, phone),
                    (rs) ->
                    {
                        HolderWrapper h = null;
                        try {
                            h = new HolderWrapper(rs.getString("holder_name"), rs.getString("holder_phone"),
                                    rs.getString("equipment_title"), rs.getString("equipment_color"),
                                    rs.getString("storage_cell_code"), rs.getString("storage_cell_capacity"));
                        } catch (SQLException e) {
                            System.out.println(e.getMessage());}
                        return h;
                    });
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
