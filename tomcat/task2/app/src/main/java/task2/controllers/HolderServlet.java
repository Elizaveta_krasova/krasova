package task2.controllers;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.net.*;
import javax.servlet.annotation.WebServlet;
import java.sql.*;
import java.util.Properties;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.io.IOException;
import javax.servlet.ServletException;

/**
  * Kласс сервлет для отображения holder
  * @author Elizaveta Krasova
  * @version %I%, %G%
  */
@WebServlet("/holders/*")
public class HolderServlet extends HttpServlet
{

    /**
     * метод, возвращающий номер телефона их адресной строки,
     * @param request
     * @return String
     */
    private String getPhone(HttpServletRequest request) {
      Matcher matcher = Pattern.compile("(\\d+\\z)").matcher(request.getRequestURI());
      matcher.find();
      return matcher.group();
    }

    /**
     * метод, который отправляет данные в holder.jsp для дальнейшего отображения
     * отображает холдера с заданным номером телефона
     * @param request
     * @param response
     * @param holder
     * @param text
     * @throws IOException
     * @throws SQLException
     * @throws ServletException
     */
    private void htmlOutput(HttpServletRequest request, HttpServletResponse response,
        Holder holder, String text) throws IOException, SQLException, ServletException {
        request.setAttribute("text", text);
        if (holder != null) {
            request.setAttribute("holder", holder);
        }
        request.getRequestDispatcher("/holder.jsp").forward(request, response);
    }

    /**
     * метод, обрабатываающий GET запрос,
     * отображает холдера с заданным номером телефона
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
         throws IOException, ServletException {
           response.setContentType("text/html; charset=UTF-8");
           response.setCharacterEncoding("UTF-8");
           request.setCharacterEncoding("UTF-8");
           PrintWriter out = response.getWriter();
           try{
              Holder holder = Holder.getbyPhone(getPhone(request));
              if (holder != null) {
                  htmlOutput(request, response, holder, "Holder");
              }
              else {
                  htmlOutput(request, response, null, "Holder not found");
              }
           } catch (Exception ex) {
               ex.printStackTrace();
           }
    }

    /**
     * метод обрабатывающий PUT запрос,
     * обновляющий холдераа с полученным номером телефона
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doPut(HttpServletRequest request, HttpServletResponse response)
                            throws IOException, ServletException {
        try {
          String numberPhone = getPhone(request);
          if (Holder.update(numberPhone, request.getParameter("name"))) {
              htmlOutput(request, response, null, "Holder update");
          } else {
              htmlOutput(request, response, null, "Holder not update");
          }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * метод, обрабатывающий DELETE запрос,
     * Удаляет холдера по номеру телефона
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doDelete(HttpServletRequest request, HttpServletResponse response)
                            throws IOException, ServletException {
        try{
            String numberPhone = getPhone(request);
            if (Holder.deletebyPhone(numberPhone) && numberPhone != null) {
                htmlOutput(request, response, null, "Delete Holder with phone " + numberPhone);
            } else {
                htmlOutput(request, response, null, "Not delete Holder with phone " + numberPhone);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
