package task2.controllers;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;

/**
  * Kласс сервлет для отображения отчета
  * @author Elizaveta Krasova
  * @version %I%, %G%
  */
@WebServlet("/holders_report")
public class HoldersReportServlet extends HttpServlet
{

    /**
     * метод, обрабатывающий GET запрос и
     * передающий в holders_report.jsp массив Holder
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
                                    throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");

        String phone = request.getParameter("phone");
        ArrayList<HolderWrapper> wrappers = QueryObject.getHolderQuery(phone);

        request.setAttribute("DTO", wrappers.toArray(new HolderWrapper[0]));
        request.getRequestDispatcher("/holders_report.jsp").forward(request, response);
    }

}
