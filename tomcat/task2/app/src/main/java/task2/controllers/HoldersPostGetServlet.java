package task2.controllers;

import app.Holder;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.net.*;
import javax.servlet.annotation.WebServlet;
import java.sql.*;
import java.util.Properties;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;

/**
  * Kласс сервлет для холдеров
  * @author Elizaveta Krasova
  * @version %I%, %G%
  */
@WebServlet("/holders")
public class HoldersPostGetServlet extends HttpServlet
{

    /**
     * метод, который отправляет данные в holder.jsp для дальнейшего отображения
     * отображает холдера с заданным номером телефона
     * @param request
     * @param response
     * @param holder
     * @param text
     * @throws IOException
     * @throws SQLException
     * @throws ServletException
     */
    private void htmlOutput(HttpServletRequest request, HttpServletResponse response,
        ArrayList<Holder> holders, String text) throws IOException, SQLException, ServletException {
        request.setAttribute("text", text);
        request.setAttribute("holders", holders.toArray(new Holder[0]));
        request.getRequestDispatcher("/holders.jsp").forward(request, response);
    }

    /**
     * метод, обрабатываающий POST запрос,
     * создаёт холдера
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
        try {
          String phone = request.getParameter("phone");
          String name = request.getParameter("name");
          if (name == null || phone == null || Holder.getbyPhone(phone) != null) {
              response.sendError(422);
          }
          if (Holder.create(phone, name)) {
              ArrayList<Holder> h = new ArrayList<Holder>();
              h.add(Holder.getbyPhone(phone));
              htmlOutput(request, response, h, "Holder create");
          } else {
              htmlOutput(request, response, null, "Holder not create");
          }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * метод, обрабатываающий GET запрос,
     * отображает всех холдеров
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
          throws IOException, ServletException {
        try {
            htmlOutput(request, response, Holder.getAll(), "holders");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
