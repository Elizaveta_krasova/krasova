package app;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.net.*;
import javax.servlet.annotation.WebServlet;
import java.util.regex.Pattern;
import java.util.Base64;
import java.util.regex.Matcher;
import java.sql.SQLException;

@WebServlet("/holder_deserialization/*")
public class HolderDeserializationServlet extends HttpServlet {

    private static int getId(HttpServletRequest request) {
        Matcher matcher = Pattern.compile("(\\d+\\z)").matcher(request.getRequestURL().toString());
        matcher.find();
        return Integer.parseInt(matcher.group());
    }

    private static void htmlOutput(HttpServletResponse response, HttpServletRequest request, Holder holder)
                                                  throws IOException, ServletException {
        request.setAttribute("holder", holder);
        request.getRequestDispatcher("/holder_deserialization.jsp").forward(request, response);
    }

    private static Holder deserialize(String hol) throws IOException {
        byte[] data = Base64.getDecoder().decode(hol);
        Holder holder = null;
        try (ObjectInputStream input = new ObjectInputStream(new ByteArrayInputStream(data))) {
            holder = (Holder)input.readObject();
        } catch (ClassNotFoundException e) {}
        return holder;
    }

    @Override
    public void doGet(HttpServletRequest request,HttpServletResponse response)
                                        throws IOException, ServletException {
        try {
            int id = getId(request);
            if (id != -1) {
                SerializedHolder holderSerialization = SerializedHolder.findById(id);
                if (holderSerialization != null) {
                    htmlOutput(response, request, deserialize(holderSerialization.getSerialize()));
                } else {
                    Holder holder = Holder.findById(id);
                    if (holder != null) {
                        htmlOutput(response, request, holder);
                    }
                }
            }
        } catch (Exception e) {}
    }

}
