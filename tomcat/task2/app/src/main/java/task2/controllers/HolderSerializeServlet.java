package app;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.net.*;
import javax.servlet.annotation.WebServlet;
import java.util.regex.Pattern;
import java.util.Base64;
import java.util.regex.Matcher;
import java.sql.SQLException;

@WebServlet("/holder_serialize/*")
public class HolderSerializeServlet extends HttpServlet {

    private static int getId(HttpServletRequest request) {
        Matcher matcher = Pattern.compile("(\\d+\\z)").matcher(request.getRequestURL().toString());
        matcher.find();
        return Integer.parseInt(matcher.group());
    }

    private static String serialize(Serializable object) throws IOException {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        try (ObjectOutputStream output = new ObjectOutputStream(b)) {
            output.writeObject(object);
        } catch (IOException e) {}
        return Base64.getEncoder().encodeToString(b.toByteArray());
    }

    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response)
                                          throws IOException, ServletException{
        try {
            Holder holder = Holder.findById(getId(request));
            if (holder != null) {
                (new SerializedHolder(holder.getId(), serialize(holder))).saveHolder();
                request.getRequestDispatcher("/holders_serialization.jsp").forward(request, response);
            }
        } catch (Exception e) {}
    }

}
