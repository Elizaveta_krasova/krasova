package app;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.net.*;
import javax.servlet.annotation.WebServlet;
import java.util.regex.Pattern;
import java.util.Base64;
import java.util.regex.Matcher;
import java.sql.SQLException;

@WebServlet("/holders_serialization")
public class HoldersSerializationServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
                                          throws IOException,ServletException {
        request.getRequestDispatcher("/holders_serialization.jsp").forward(request, response);
    }

}
