package app;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SerializedHolder {

    private String serialize;
    private int id;
    private static final String[] COLUMNS = new String[]{"id", "serialize"};
    private static final String TABLE_NAME = "serialized_holder";
    private static final ConnectionDB<SerializedHolder> conn = new ConnectionDB<>();

    private static final ConnectionDB.ImageLambda<SerializedHolder> lambda = (rs) ->
    {
        SerializedHolder holder = null;
        try {
            holder = new SerializedHolder(rs.getInt("id"), rs.getString("serialize"));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return holder;
    };

    PreparedStatement stmtUpdate = ConnectionDB.getPreparedStatement(
          String.format("UPDATE %s SET serialize = ? WHERE id = ?", TABLE_NAME));

    SerializedHolder(int id, String serialize) {
        this.id = id;
        this.serialize = serialize;
    }

    public int getHolderId() {
        return id;
    }

    public void setSerialize(String serialize) {
        this.serialize = serialize;
    }

    public String getSerialize() {
        return serialize;
    }

    public static int createHolder(int id, String serialize) throws SQLException{
        PreparedStatement stmtCreate = ConnectionDB.getPreparedStatement(
            String.format("INSERT INTO %s (%s) VALUES(?, ?)", TABLE_NAME, String.join(", ", COLUMNS)),
            new String[]{"id"});
        int newId = -1;
        try {
            stmtCreate.setInt(1, id);
            stmtCreate.setString(2, serialize);
            stmtCreate.executeUpdate();
            ResultSet rs = stmtCreate.getGeneratedKeys();
            if (rs.next()) {
                newId = rs.getInt("id");
            }
        } catch (SQLException e) {}
        return newId;
    }

    public boolean saveHolder() throws SQLException{
        SerializedHolder holder = findById(id);
        if (holder != null){
            try {
                stmtUpdate.clearParameters();
                stmtUpdate.setInt(1, id);
                stmtUpdate.setString(2, serialize);
                stmtUpdate.executeUpdate();
            } catch (SQLException e) {}
        } else {
            id = createHolder(id, serialize);
            holder = findById(id);
        }
        return holder.getHolderId() == id && holder != null && holder.getSerialize().equals(serialize);
    }

    public static SerializedHolder findById(int id) throws SQLException{
        ArrayList<SerializedHolder> hol = conn.executeSelect(String.format(
        "SELECT %s FROM %s WHERE id = %s", String.join(", ", COLUMNS), TABLE_NAME, id), lambda);
        return hol.isEmpty() ? null : hol.get(0);
    }

    private static ArrayList<SerializedHolder> getAllHolders(String query) throws SQLException{
        return conn.executeSelect(query, lambda);
    }
}
