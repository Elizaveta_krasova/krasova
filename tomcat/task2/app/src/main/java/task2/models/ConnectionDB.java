package task2.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ArrayList;

public class ConnectionDB<T> {

    private static Connection conn;
    private static final String URL = "jdbc:postgresql://127.0.0.1:5432/postgres";
    private static final String PASSWORD = "Veronika.1968";
    private static final String USER = "postgres";

    public interface ImageLambda<E>{
      E modelClass(ResultSet rs);
    }

    public static Connection createConn() throws SQLException{
        Properties connectionProps = new Properties();
        connectionProps.put("user", USER);
        connectionProps.put("password", PASSWORD);
        conn = DriverManager.getConnection(URL, connectionProps);
        return conn;
    }

    public static Connection getConn() throws SQLException{
        if (conn == null){
          conn = createConn();
        }
        return conn;
    }

    public static ResultSet executeWithReturn(String query) throws SQLException {
        return getConn().createStatement().executeQuery(query);
    }

    public static void executeWithoutReturn(String query) throws SQLException {
        getConn().createStatement().executeUpdate(query);
    }

    public ArrayList<T> executeSelect(String query, ImageLambda<T> lambda) throws SQLException{
        ArrayList<T> arrayList = new ArrayList<>();
        ResultSet rs = executeWithReturn(query);
        while (rs.next()){
            arrayList.add(lambda.modelClass(rs));
        }
        return arrayList;
    }


    public static PreparedStatement getPreparedStatement(String query){
        PreparedStatement stmt = null;
        try {
            stmt = getConn().prepareStatement(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return stmt;
    }

    public static PreparedStatement getPreparedStatement(String query, String[] generatedColumns){
        PreparedStatement stmt = null;
        try {
            stmt = getConn().prepareStatement(query, generatedColumns);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return stmt;
    }

}
