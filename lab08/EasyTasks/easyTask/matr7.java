import java.util.Scanner;
public class Matr7{

  public static void main(String[] args){
    System.out.println("Введите размер матрицы");
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int m = in.nextInt();
    int[][] matrixA;
    matrixA = new int[n][m];
    for (int i=0; i<n; i++){
      System.out.println("Введите строку из " + m + " чисел");
      for (int j=0; j<m; j++){
        matrixA[i][j] = in.nextInt();
      }
    }
    System.out.println("Ваша матрица в одну строчку:");
    for (int i=0; i<n; i++){
      for (int j=0; j<m; j++){
        System.out.printf("%d", matrixA[i][j]);
        System.out.print(' ');
      }
    }
  }

}
