import java.util.Scanner;
public class Matr4 {
  public static void main (String[] args){
    System.out.println("Введите размер матрицы");
    Scanner in = new Scanner(System.in);
		int n = in.nextInt();
    int[][] matrixA;
    matrixA = new int[n][n];
    for (int i=0; i<n; i++){
      System.out.println("Введите строку из " + n + " чисел");
      for (int j=0; j<n; j++){
        matrixA[i][j] = in.nextInt();
      }
    }
    System.out.println("Числа, находящиеся ниже главной диагонали");
    for (int i=0; i<n; i++){
      for (int j=0; j<n; j++){
        if (i>j){
          System.out.printf("%d", matrixA[i][j]);
          System.out.print(' ');
        }
        else{
          System.out.print('*');
          System.out.print(' ');
        }
      }
      System.out.println(' ');
    }
  }
}
