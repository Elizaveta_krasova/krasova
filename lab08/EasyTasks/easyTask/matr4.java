import java.util.Scanner;
public class Matr4 {
  public static void main (String[] args){

    System.out.println("Введите размер матрицы");
    Scanner in = new Scanner(System.in);
		int n = in.nextInt();
    int m = in.nextInt();

    int[][] matrixA;
    matrixA = new int[n][m];

    for (int i=0; i<n; i++){
      System.out.println("Введите строку из " + m + " чисел");
      for (int j=0; j<m; j++){
        matrixA[i][j] = in.nextInt();
      }
    }

    System.out.println("Транспонированная матрица");

    for (int i=0; i<m; i++){
      for (int j=0; j<n; j++){
        System.out.printf("%d", matrixA[j][i]);
        System.out.print(' ');
      }

    System.out.println(' ');
    }
  }
}
