import java.util.Scanner;
public class Factorial1 {
	static int factorial1(int a){
		if (a==1){
			return 1;
		}
		return a*factorial1(a-1);
	}
	public static void main(String[] args){
		Scanner sc =  new Scanner(System.in);
		int a;
		System.out.println("Факториал какого числа нужно вычислить?");
		a=sc.nextInt();
		System.out.println(factorial1(a));
	}
}
