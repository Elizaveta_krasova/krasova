package Task2;
import java.util.Scanner;
import java.util.*;
import java.lang.*;

public class Fibonachi1{
  // Через цикл
    public static int[] fibonachi2(int a){
      int[] array = new int[a];
      if (a == 0){
        return null;
      }
      if (a == 1){
        array[0] = 0;
        return array;
      }
      if (a == 2){
        array[0] = 0;
        array[1] = 1;
        return array;
      }
      if (a > 2){
        array[0] = 0;
        array[1] = 1;
        for (int i = 2; i < a; i++){
          array[i] = array[i-2] + array[i-1];
        }
      }
     return array;
   }
// Через рекурсию (Мемоизация)
   public static int fibonachi(int a, int[] massiv){
      if (massiv[a - 1] != 0){
        return massiv[a - 1];
      }
      if (a <= 2){
        massiv[a - 1] = a - 1;
        return massiv[a - 1];
      }
      int result = fibonachi(a - 1, massiv) + fibonachi(a - 2, massiv);
      massiv[a - 1] = result;
      return result;
   }

   public static int[] getFibonachi(int n){
     if (n == 0){
       return null;
     }
     int[] massiv_new = new int[n];
     fibonachi(n, massiv_new);
     return massiv_new;
   }

  public static void main(String[] args){
    int n = 20;
    System.out.println(Arrays.toString(getFibonachi(n)));

    int[] array2 = new int[n];
    array2 = fibonachi2(n);
    System.out.println(Arrays.toString(array2));
  }
}
