package Task2;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FibonachiTest {
    @Test void checkNormalFibonachi(){
      int[] array1 = new int[]{0, 1, 1, 2, 3, 5, 8, 13, 21, 34};
      assertArrayEquals(array1, Fibonachi1.getFibonachi(10), "work correct");
    }

    @Test void checkEmptyFibonachi(){
      assertArrayEquals(null, Fibonachi1.getFibonachi(0), "work correct");
    }

    @Test void checkNormalFibonachi2(){
      int[] array = new int[]{0, 1, 1, 2, 3, 5, 8, 13, 21, 34};
      assertArrayEquals(array, Fibonachi1.fibonachi2(10), "work correct");
    }

    @Test void checkEmptyFibonachi2(){
      assertArrayEquals(null, Fibonachi1.fibonachi2(0), "work correct");
    }

}
