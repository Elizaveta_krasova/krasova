package Task1;
import java.util.Scanner;

public class Faktorial{

  public static int factorial(int a){
    if (a==1){
      return 1;
    }
    return a*factorial(a-1);
  }

  public static void main(String[] args){
    Scanner sc =  new Scanner(System.in);
    int a;
    System.out.println("Факториал какого числа нужно вычислить?");
    a=sc.nextInt();
    System.out.println(factorial(a));
  }
}
