package Task2;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class GeneratorTest {
    @Test void checkShortArray(){
      Generator generator = new Generator(100, 5);
      generator.generate();
      assertEquals(true, generator.generatedInTime(), "work correct");
    }

    @Test void checkLongArray(){
      Generator generator = new Generator(1000000, 5);
      generator.generate();
      assertEquals(false, generator.generatedInTime(), "work correct");
    }

    @Test void checkEmptyArray(){
      Generator generator = new Generator(0, 5);
      generator.generate();
      assertEquals(true, generator.generatedInTime(), "work correct");
    }
}
