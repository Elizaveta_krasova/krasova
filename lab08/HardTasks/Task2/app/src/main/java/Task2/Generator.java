package Task2;
import java.util.Date;

class Generator{

  private int[] randomNumbers;
  private long timeToGenerate;
  private int timeThresold;
  private int count;

  Generator(int count, int timeThresold){
    this.count = count;
    this.timeThresold = timeThresold;
    this.randomNumbers = new int[this.count];
    this.generate();
  }

  public void generate(){
    long start = new Date().getTime();
    for (int i = 0; i < this.count; i++){
      this.randomNumbers[i] = (int)(Math.random()*100);
    }
    this.timeToGenerate = (new Date().getTime() - start);
  }

  public boolean generatedInTime(){
    return this.timeToGenerate < this.timeThresold;
  }

  public int[] getRandomNumbers(){
    return this.generatedInTime() ? this.randomNumbers : null;
  }
}
