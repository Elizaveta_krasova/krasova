package Task2;
import java.util.Date;

class Program2{

  public static void printNumbers(int[] array){
    for (int i = 0; i < array.length - 1; i++){
      System.out.format("%d, ", array[i]);
    }
    System.out.print(array[array.length - 1]);
  }

  public static void main(String[] args) {
    Generator generator = new Generator(100000, 5);
    generator.generate();
    if (generator.generatedInTime()) {
      printNumbers(generator.getRandomNumbers());
    } else {
      System.out.println("Превышено допустимое время работы программы");
    }
  }

}
