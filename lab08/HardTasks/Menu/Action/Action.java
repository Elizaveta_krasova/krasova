package Menu.Action;

abstract public class Action{
  String str;

  Action(String str){
    this.str = str;
  }

  abstract public void act();

  public String getStr(){
    return this.str;
  }
}
