package Menu;
import java.util.Scanner;
import Menu.Action.*;

class Main{
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int userResponse = 0;
    Question begin = new Question();
    Question level1 = new Question();
    Question level1_2 = new Question();
    Question level2 = new Question();
    Question level2_2 = new Question();
    Action one = new ActionOne("Вывести предсказание 1");
    begin.connection(level1);
    begin.connection(level1_2);
    begin.setAction(one);
    level1.setAction(new ActionTwo("Вывести предсказание 2"));
    level1.connection(level2);
    level1.connection(level2_2);
    level1_2.setAction(new ActionThree("Вывести предсказание 3"));
    level2.setAction(new ActionFour("Вывести предсказание 4"));
    level2_2.setAction(new ActionFife("Вывести предсказание 5"));

    while(true){
      begin.printStr();
      userResponse = in.nextInt();
      System.out.println();
      if (userResponse == 0){
         if (begin.parent == null){
              break;
        }
         else begin = begin.parent;
      }
      if (userResponse == 3){
        begin.actions.get(userResponse - 3).act();
      }
      if(userResponse == 1 && begin.children.size() == 0){
        begin.actions.get(userResponse - 1).act();
      }
      if(userResponse > 0 && userResponse < begin.actions.size() + begin.children.size()){
          begin = begin.children.get(userResponse - begin.children.size() + 1);
      }
    }
  }
}
